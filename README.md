# ChitChat #

ChitChat helps classifing texts with labels/tags by using matching rules. It also lets you define feedback in terms of those labels. Those are nice tools to do trend analysis in social media or to create chat bots.

[Take a look at the wiki](https://bitbucket.org/arvid/chitchat/wiki/Home)

Leiden university - Centre for Innovation 

Arvid Halma, Boaz Manger, Wouter Eekhout