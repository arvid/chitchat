
# Install Ubuntu # 

- use US locale
- use encryption
- guided partition LVM encrypted
- install security updates automatically
- install NO software other then basics



to correct timezone
> tzselect

Set lang (avoid Perl error)
> vim .bashrc
add...
export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8

source .bashrc


## Install tools ##
```
sudo apt-get install openssh-server openssh-client
sudo apt-get install vim
sudo apt-get install screen
sudo apt-get install curl
sudo apt install unzip
sudo apt install htop
sudo apt install git
sudo apt install software-properties-common
sudo apt install net-tools
sudo apt-get install openjdk-8-jdk
```

## Copy ssh keys ##

```
ssh-copy-id -i ~/.ssh/mykey user@host
```

## PostgreSQL ##

```
sudo apt-get install postgresql-10

sudo -u postgres psql -p 5432

CREATE USER chitchat WITH PASSWORD 'secret';
CREATE DATABASE chitchat OWNER chitchat;
\connect chitchat
CREATE EXTENSION "uuid-ossp";
CREATE EXTENSION pgcrypto;
```

Initialize DB schema:

```
java -jar chitchat-x.x.x.jar db migrate mysettings.yml

```

Export textdoc table:

```
sudo su - postgres
pg_dump --host localhost --port 5432 --username chitchat --format plain --verbose --file "textdoc.bak" --data-only --table public.textdoc chitchat
```

Restore:

```
psql chitchat < textdoc.bak
```

## nginx ##
```
sudo apt-get install nginx
```
Check:
```
systemctl status nginx

sudo systemctl stop nginx
```

## Let's encrypt ##

Make a file `sudo vim /etc/nginx/conf.d/default.conf`


```
server {
        listen 80;
        server_name chitchat.cadmium.blue;
        
        location / {
                proxy_pass http://localhost:5678;
                #auth_basic "Restricted";                                #For Basic Auth
                #auth_basic_user_file /etc/nginx/.htpasswd_chitchat;  #For Basic Auth
        }
        location ~ /.well-known {
                allow all;
        }

 
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/chitchat.cadmium.blue/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/chitchat.cadmium.blue/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot


    if ($scheme != "https") {
        return 301 https://$host$request_uri;
    } # managed by Certbot

}

```
https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-20-04

Deprecated: 
https://www.linuxcloudvps.com/blog/how-to-install-lets-encrypt-on-ubuntu-16-04-with-nginx/

```
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install python-certbot-nginx

sudo certbot --nginx -d domain.com
sudo crontab -e
```
add..
```
* */12 * * * /usr/bin/certbot renew >/dev/null 2>&1 
```

## Docker ##

Install docker: https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce-1

Add line to `/etc/postgresql/10/main/pg_hba.conf` in section `# IPv4 local connections:`

```
host    all             all             172.17.0.0/16            md5
```

Extend list of allowed clients in `/etc/postgresql/10/main/postgresql.conf`:

```
listen_addresses = 'localhost,172.17.0.0'
```

or:

```
listen_addresses = '*'
```

Make sure Chitchat `settings.yml` connects to the external `database:`:

```
url: jdbc:postgresql://database:5436/chitchat
```

Build docker:
```
sudo docker build -t chitchatdocker .
```

Run docker (fill in local host IP address):

```
sudo docker run -it --add-host="database:<HOST_IP>" chitchatdocker
```