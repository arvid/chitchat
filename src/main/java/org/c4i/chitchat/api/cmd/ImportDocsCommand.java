package org.c4i.chitchat.api.cmd;

import io.dropwizard.cli.EnvironmentCommand;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Environment;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;
import org.apache.commons.io.FileUtils;
import org.c4i.chitchat.api.App;
import org.c4i.chitchat.api.Config;
import org.c4i.chitchat.api.db.TextDocDao;
import org.c4i.chitchat.api.model.TextDoc;
import org.jdbi.v3.core.Jdbi;

import java.io.File;

/**
 * Import data
 * @author Arvid Halma
 */
public class ImportDocsCommand extends EnvironmentCommand<Config> {

    public ImportDocsCommand() {
        super( new App(),"importdocs", "Import documents/text files to analyse");
    }

    @Override
    public void configure(Subparser subparser) {
        subparser.addArgument("-s", "--src")
                .dest("src")
                .required(true)
                .type(String.class)
                .help("The file or directory from which the text should be imported");
        subparser.addArgument("-t", "--doctype")
                .dest("doctype")
                .setDefault("doc")
                .required(false)
                .type(String.class)
                .help("The type (or collection label) assigned in the textdoc DB table for the specified documents");
        super.configure(subparser);
    }

    @Override
    protected void run(Environment environment,
                       Namespace namespace,
                       Config config) throws Exception
    {
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, config.getDataSourceFactory(), "database");

        // DAOs
        final TextDocDao textDocDao = jdbi.onDemand(TextDocDao.class);


        final File file = new File(namespace.getString("src"));
        String doctype = "doc/"+namespace.getString("doctype");

        File[] ls = new File[]{file};
        if (file.isDirectory()) {
            ls = file.listFiles(File::isFile);
        }

        int nSuccess = 0;
        int nFail = 0;
        for (int i = 0; i < ls.length; i++) {
            File f = ls[i];
            if(!f.getName().endsWith(".txt"))
                continue;
            try {
                final String body = FileUtils.readFileToString(f, "UTF-8");
                System.out.println("body: " + body.substring(0, Math.min(body.length(), 30)).replace('\n', ';') + "...");

                textDocDao.upsert(new TextDoc()
                        .setId(f.getName())
                        .setName(f.getName())
                        .setType(doctype)
                        .setBody(body));
                nSuccess++;
            } catch (Exception e) {
                System.err.println(f + ": " + e.getMessage());
                nFail++;
            }
        }
        System.out.println("=====================");
        System.out.println("Number of documents successfully imported:" + nSuccess);
        System.out.println("Number of documents failed to import:" + nFail);
        System.exit(0);
    }

    /*@Override
    protected void run(Environment environment,
                       Namespace namespace,
                       Config config) throws Exception
    {
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, config.getDataSourceFactory(), "database");

        // DAOs
        final TextDocDao textDocDao = jdbi.onDemand(TextDocDao.class);

        AutoDetectParser parser = new AutoDetectParser();
        Metadata metadata = new Metadata();
        final File file = new File(namespace.getString("src"));
        String doctype = "doc/"+namespace.getString("doctype");

        File[] ls = new File[]{file};
        if (file.isDirectory()) {
            ls = file.listFiles(File::isFile);
        }

        int nSuccess = 0;
        int nFail = 0;
        for (int i = 0; i < ls.length; i++) {
            File f = ls[i];
            try (InputStream stream = new FileInputStream(f)) {
                BodyContentHandler handler = new BodyContentHandler(-1);
                parser.parse(stream, handler, metadata);
                System.out.println("=====================");
                System.out.println("File " + i + ": " + f);

                final String body = handler.toString();

                System.out.println("body: " + body.substring(0, Math.min(body.length(), 30)).replace('\n', ';') + "...");

                textDocDao.upsert(new TextDoc()
                        .setId(f.getName())
                        .setName(f.getName())
                        .setType(doctype)
                        .setBody(body));
                nSuccess++;
            } catch (TikaException | SAXException e) {
                System.err.println(f + ": " + e.getMessage());
                nFail++;
            }
        }
        System.out.println("=====================");
        System.out.println("Number of documents successfully imported:" + nSuccess);
        System.out.println("Number of documents failed to import:" + nFail);
        System.exit(0);


    }*/


}
