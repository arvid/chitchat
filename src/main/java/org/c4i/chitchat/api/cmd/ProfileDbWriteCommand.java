package org.c4i.chitchat.api.cmd;


import io.dropwizard.cli.EnvironmentCommand;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Environment;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;
import org.apache.commons.lang3.time.StopWatch;
import org.c4i.chitchat.api.App;
import org.c4i.chitchat.api.Config;
import org.c4i.chitchat.api.db.ConversationDao;
import org.c4i.nlp.chat.Conversation;
import org.c4i.nlp.chat.Message;
import org.jdbi.v3.core.Jdbi;

/**
 * Import data
 * @author Arvid Halma
 */
//public class ProfileDbWriteCommand extends EnvironmentCommand<Config> {
public class ProfileDbWriteCommand extends EnvironmentCommand<Config> {

    public ProfileDbWriteCommand() {
        super( new App(),"profiledbwrite", "Timinging info for DB writes");
    }

    @Override
    public void configure(Subparser subparser) {
        subparser.addArgument("-n", "--number")
                .dest("number")
                .required(true)
                .type(Integer.class)
                .help("The number of converstations to be inserted");
        super.configure(subparser);
    }

    @Override
    protected void run(Environment environment,
                       Namespace namespace,
                       Config config) throws Exception
    {
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, config.getDataSourceFactory(), "database");

        // DAOs
        final ConversationDao conversationDao = jdbi.onDemand(ConversationDao.class);

        int n = namespace.getInt("number");

        String[] msgs = {"message 1", "message 2", "message 3" };

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        System.out.println(" Start");
        for (int i = 0; i < n; i++) {

            if(i % 1000 == 0){
                System.out.println("i = " + i);
            }

            final Conversation conv = new Conversation("speedtest-"+i);

            for (String msg : msgs) {
                Message m = new Message();
                m.setConversationId(conv.getId());
                m.setText(msg);
                m.setIncoming(true);
                conv.getMessages().add(m);

                conversationDao.upsertConversation(conv);
                conversationDao.upsertMessage(m);
            }
        }
        stopWatch.stop();

        System.out.println( n + " conversation updates = " + stopWatch);
        System.out.println("conversationDao = " + conversationDao.messageCount());

        conversationDao.deleteConversationsLike("speedtest-%");
//        conversationDao.deleteConversationsLike("speedtest-%");

        System.exit(0);


    }


}
