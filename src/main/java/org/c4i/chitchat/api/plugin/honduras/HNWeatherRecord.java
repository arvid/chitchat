package org.c4i.chitchat.api.plugin.honduras;

import org.c4i.util.time.Timestamped;
import org.joda.time.DateTime;

import java.util.Objects;

/**
 * ID	Fecha	Bateria	HumedadSuelo	HumedadAmbiente	TemperaturaAmbiente	VelocidadViento	DireccionViento	Lluvia	Luxometro
 * 1	2019-09-11 06:13:10	96.75	0	41	33.11	2.37	73	0	51453
 * 2	2019-09-11 06:17:45	96.75	0	43	31.82	3.13	22	0	50659
 * 3	2019-09-11 06:22:17	96.75	0	42	32.72	2.82	138	0	49866
 */
public class HNWeatherRecord implements Timestamped {
    public long id;
    public DateTime fecha;
    public double bateria;
    public double humedadSuelo;
    public double humedadAmbiente;
    public double temperaturaAmbiente;
    public double velocidadViento;
    public double direccionViento;
    public double lluvia;
    public double luxometro;

    public HNWeatherRecord() {
    }

    public HNWeatherRecord(long id, DateTime fecha, double bateria, double humedadSuelo, double humedadAmbiente, double temperaturaAmbiente, double velocidadViento, double direccionViento, double lluvia, double luxometro) {
        this.id = id;
        this.fecha = fecha;
        this.bateria = bateria;
        this.humedadSuelo = humedadSuelo;
        this.humedadAmbiente = humedadAmbiente;
        this.temperaturaAmbiente = temperaturaAmbiente;
        this.velocidadViento = velocidadViento;
        this.direccionViento = direccionViento;
        this.lluvia = lluvia;
        this.luxometro = luxometro;
    }

    public double get(String colName){
        switch (colName){
            case "Luxometro": return luxometro;
            case "VelocidadViento": return velocidadViento;
            case "Lluvia": return lluvia;
            case "HumedadAmbiente": return humedadAmbiente;
            case "TemperaturaAmbiente": return temperaturaAmbiente;
        }
        return Double.NaN;
    }

    @Override
    public DateTime getTimestamp() {
        return fecha;
    }

    @Override
    public String toString() {
        return "HNWeatherRecord{" +
                "id=" + id +
                ", fecha=" + fecha +
                ", bateria=" + bateria +
                ", humedadSuelo=" + humedadSuelo +
                ", humedadAmbiente=" + humedadAmbiente +
                ", temperaturaAmbiente=" + temperaturaAmbiente +
                ", velocidadViento=" + velocidadViento +
                ", direccionViento=" + direccionViento +
                ", lluvia=" + lluvia +
                ", luxometro=" + luxometro +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HNWeatherRecord that = (HNWeatherRecord) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
