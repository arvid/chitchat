package org.c4i.chitchat.api.plugin.peru;

import org.c4i.util.Histogram;
import org.c4i.util.StringUtil;
import org.c4i.util.time.TimeValue;
import org.c4i.util.time.Timeline;
import org.joda.time.DateTime;
import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.style.Styler;
import org.knowm.xchart.style.XYStyler;
import org.knowm.xchart.style.markers.SeriesMarkers;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Plot price time series data to a PNG file.
 * @author Arvid Halma
 * @version 11-11-19
 */
public class PeruPricePlotX {

    public PeruPricePlotX() {
    }

    public static void plot(Map<String, Timeline<TimeValue>> timelines, File file) throws IOException {
//        Histogram<String> mercados = new Histogram<>(timelines.keySet().stream().map(key -> key.split(" \\| ")[1]));
//        Histogram<String> variedades = new Histogram<>(timelines.keySet().stream().map(key -> key.split(" \\| ")[0]));

        Set<String> mercados = new HashSet<>();
        Map<String, Set<String>> varToMercados = new HashMap<>();
        for (String varMcdo : timelines.keySet()) {
            String[] key = varMcdo.split(" \\| ");
            String variedad = key[0];
            String mcdo = key[1];

            mercados.add(mcdo);
            if(!varToMercados.containsKey(variedad)){
                varToMercados.put(variedad, new HashSet<>());
            }
            varToMercados.get(variedad).add(mcdo);

        }

        // Create Chart
        int height = Math.max(500, 32*timelines.size()+150); // ensure legend fits
        height = Math.min(height, 1000); // but don't go crazy
        XYChart chart = new XYChart(800, height);
        final XYStyler styler = chart.getStyler();

        if(varToMercados.size() == 1) {
            String variedad = varToMercados.keySet().iterator().next();
            chart.setTitle("Precios de " + StringUtil.truncate(StringUtil.properCase(variedad), 28));
            styler.setLegendVisible(mercados.size() > 1);
        } else {
            chart.setTitle("Comportamiento del precio");
            styler.setLegendVisible(true);
        }


        chart.setXAxisTitle(""); // Dates are clear from the axis labels
        // chart.setXAxisTitle("Fecha");
        chart.setYAxisTitle("Soles por kg");

        // Customize Chart
        styler.setLegendPosition(Styler.LegendPosition.InsideSW);
        styler.setLegendBackgroundColor(new Color(1f,1f,1f,.7f )); // transparent white
        styler.setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);
        styler.setYAxisLabelAlignment(Styler.TextAlignment.Right);
        styler.setPlotMargin(0);
        styler.setXAxisLabelRotation(-45);
        styler.setChartBackgroundColor(Color.WHITE);
        DateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.forLanguageTag("es"));
        styler.setDatePattern("dd-MMM");
        styler.setDecimalPattern("#0.00");

        Font titleFont = new Font(Font.SANS_SERIF, Font.BOLD, 36);
        Font fontAxis = new Font(Font.SANS_SERIF, Font.PLAIN, 24);
        Font fontLegend = new Font(Font.SANS_SERIF, Font.PLAIN, 20);
        Font font = new Font(Font.SANS_SERIF, Font.PLAIN, 24);
        styler.setChartTitleFont(titleFont);
        styler.setLegendFont(fontLegend);
        styler.setAxisTitleFont(fontAxis);
        styler.setAxisTickLabelsFont(font);
        //styler.setLegendSeriesLineLength(12);

        // Generates linear data
        for (Map.Entry<String, Timeline<TimeValue>> entry : timelines.entrySet()) {
            String[] key = entry.getKey().split(" \\| ");
            String variedad = key[0];
            String mcdo = key[1];

            Timeline<TimeValue> line = entry.getValue();

            List<Date> xData = new ArrayList<>();
            List<Double> yData = new ArrayList<>();

            for (TimeValue timeValue : line) {
                DateTime t = timeValue.getTimestamp();
                xData.add(t.toDate());
                yData.add(timeValue.getValue());
            }

            String label = entry.getKey();
            if(varToMercados.size() == 1) {
                label = mcdo;
            } else if(varToMercados.get(variedad).size() == 1){
                label = variedad;
            }
            final XYSeries series = chart.addSeries(StringUtil.properCase(label), xData, yData);
            series.setMarker(SeriesMarkers.CIRCLE);
        }

        // Write to file
        BitmapEncoder.saveBitmap(chart, file.getPath(), BitmapEncoder.BitmapFormat.PNG);

        // Show it
        // new SwingWrapper(chart).displayChart();
    }


}
