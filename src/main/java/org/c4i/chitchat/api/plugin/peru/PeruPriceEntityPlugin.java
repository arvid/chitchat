package org.c4i.chitchat.api.plugin.peru;

import com.google.common.collect.ImmutableList;
import org.c4i.nlp.Nlp;
import org.c4i.nlp.match.EntityPlugin;
import org.c4i.nlp.match.Literal;
import org.c4i.nlp.match.Range;
import org.c4i.nlp.ner.DataSheet;
import org.c4i.nlp.tokenize.Token;

import java.util.Collection;
import java.util.List;

/**
 * Find latest prices of commodities in Peru given MINAGRI data.
 * @author Arvid Halma
 */
public class PeruPriceEntityPlugin implements EntityPlugin {
    Nlp nlp;

    public PeruPriceEntityPlugin(Nlp nlp) {
        this.nlp = nlp;
    }

    @Override
    public boolean accept(Literal lit) {
        return lit.equals("PERUPRICE");
    }

    @Override
    public List<Range> find(Token[] text, Literal lit, String label, int location, Collection<Range> context) {
        DataSheet dataSheet = nlp.getDataSheet("es", "BUSCAPRECIOSPERU");
        if(dataSheet == null){
            return ImmutableList.of();
        }

        List<Range> ranges = dataSheet.find(text, lit, label, location, context);

        // find last location filter
        /*Optional<Range> locFilter = Lists.reverse(context).stream().filter(r -> "locationFilter".equals(r.label)).findFirst();
        if(locFilter.isPresent()){
            String loc = locFilter.get().value;
            ranges = ranges.stream().filter(range -> Objects.equals(loc, range.props.get("region"))).collect(Collectors.toList());
        }*/

        return ranges;
    }

    @Override
    public String description() {
        return "PERUPRICE";
    }
}
