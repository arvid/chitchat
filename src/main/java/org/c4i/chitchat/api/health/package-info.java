/**
 * Small self-tests which the application performs to verify that a specific component or responsibility is performing correctly.
 */
package org.c4i.chitchat.api.health;