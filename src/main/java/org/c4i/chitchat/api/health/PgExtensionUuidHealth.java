package org.c4i.chitchat.api.health;

import com.codahale.metrics.health.HealthCheck;
import org.c4i.chitchat.api.Config;

/**
 * Check if the settings in settings.yml are correct.
 * @author Arvid Halma
 * @version 15-11-2015 - 12:13
 */
public class PgExtensionUuidHealth extends HealthCheck {
    private Config configuration;

    public PgExtensionUuidHealth(Config configuration) {
        this.configuration = configuration;
    }

    @Override
    protected Result check() {
        return configuration.dao.baseDao.hasUuidOssp() ? Result.healthy("Postgres extension installed") : Result.unhealthy("Postgres extension not installed");
    }
}