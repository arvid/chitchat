package org.c4i.chitchat.api.health;

import com.codahale.metrics.health.HealthCheck;
import org.c4i.chitchat.api.Config;

/**
 * Facebook channel check.
 * @author Arvid Halma
 * @version 15-11-2015 - 12:13
 */
public class TelegramHealth extends HealthCheck {
    private Config configuration;

    public TelegramHealth(Config configuration) {
        this.configuration = configuration;
    }

    @Override
    protected Result check() {
        if(configuration.telegramSettings != null) {
            try {
                configuration.telegramResource.loadLiveScript(false);
                return Result.healthy("Telegram is live");
            } catch (Exception e) {
                return Result.unhealthy("Telegram is configured, but has no live script: " + e.getMessage());
            }

        } else {
            return Result.healthy("Telegram not enabled (not defined in settings)");
        }
    }
}