package org.c4i.chitchat.api;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.google.common.collect.ImmutableList;
import de.ahus1.keycloak.dropwizard.*;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.jdbi3.bundles.JdbiExceptionsBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import org.c4i.chitchat.api.model.TwilioSettings;
import org.c4i.chitchat.api.sec.*;
import org.c4i.nlp.chat.ConversationManager;
import org.c4i.chitchat.api.cmd.CreateLoginCommand;
import org.c4i.chitchat.api.cmd.ImportDocsCommand;
import org.c4i.chitchat.api.cmd.ProfileDbWriteCommand;
import org.c4i.chitchat.api.db.*;
import org.c4i.chitchat.api.health.*;
import org.c4i.chitchat.api.model.Ace;
import org.c4i.chitchat.api.model.JsonDoc;
import org.c4i.chitchat.api.model.TextDoc;
import org.c4i.chitchat.api.resource.*;
import org.c4i.chitchat.api.sec.*;
import org.c4i.chitchat.api.sec.KeycloakAuthenticator;
import org.c4i.graph.GraphModule;
import org.c4i.nlp.chat.Conversation;
import org.c4i.nlp.chat.Message;
import org.c4i.nlp.match.Range;
import org.c4i.util.HistogramModule;
import org.c4i.util.TupleModule;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.io.IOException;
import java.security.Principal;
import java.util.EnumSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


/**
 * The Application class pulls together the various bundles and commands which provide basic functionality.
 * @author Arvid Halma
 * @version 3-4-2016
 */
public class App extends Application<Config> {
    private final Logger logger = LoggerFactory.getLogger(App.class);

    private Config config;

    public static void main(String[] args) throws Exception {
        new App().run(args);
    }

    @Override
    public String getName() {
        return "chitchat";
    }


    public Config getConfig() {
        return config;
    }

    @Override
    public void initialize(Bootstrap<Config> bootstrap) {
        // Serve static content
        bootstrap.addBundle(new AssetsBundle("/assets", "/", "index.html"));

        // Swagger API docs
        bootstrap.addBundle(new SwaggerBundle<Config>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(Config configuration) {
                return configuration.getSwaggerBundleConfiguration();
            }
        });

        // Add multipart message support
        bootstrap.addBundle(new MultiPartBundle());

        // Views
        bootstrap.addBundle(new ViewBundle<>());

        // Add CLI commands
        bootstrap.addCommand(new CreateLoginCommand());
        bootstrap.addCommand(new ProfileDbWriteCommand());
        bootstrap.addCommand(new ImportDocsCommand());

        // By adding the DBIExceptionsBundle to your application, Dropwizard will automatically
        // unwrap any thrown SQLException or DBIException instances.
        // This is critical for debugging, since otherwise only the common wrapper exception's
        // stack trace is logged.
        bootstrap.addBundle(new JdbiExceptionsBundle());

        // Add db migration support
        bootstrap.addBundle(new MigrationsBundle<Config>() {

            @Override
            public String getMigrationsFileName() {
                return "migrations.yml";
            }

            @Override
            public DataSourceFactory getDataSourceFactory(Config configuration) {
                return configuration.getDataSourceFactory();
            }
        });

        bootstrap.addBundle(new KeycloakBundle<Config>() {
            @Override
            protected KeycloakConfiguration getKeycloakConfiguration(Config config) {
                return config.getKeycloakConfiguration();
            }

            @Override
            protected Class<? extends Principal> getUserClass() {
                return KeycloakUser.class;
            }

            @Override
            protected KeycloakAuthorizer createAuthorizer() {
                return new KeycloakAuthorizer();
            }

            @Override
            protected Authenticator createAuthenticator(KeycloakConfiguration keycloakConfiguration) {
                return new KeycloakAuthenticator(keycloakConfiguration);
            }
        });
    }

    /**
     * Cross-Origin Resource Sharing (CORS) is a mechanism that uses additional HTTP headers to tell a browser to
     * let a web application running at one origin (domain) have permission to access selected resources from a server
     * at a different origin.
     * A web application makes a cross-origin HTTP request when it requests a resource that has a different origin
     * (domain, protocol, and port) than its own origin.
     *
     * @see "https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS"
     * @param environment Dropwizard environment
     */
    private void configureCors(Environment environment) {
        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "Cache-Control,If-Modified-Since,Pragma,Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        cors.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
        cors.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");

        // DO NOT pass a preflight request to down-stream auth filters
        // unauthenticated preflight requests should be permitted by spec
        cors.setInitParameter(CrossOriginFilter.CHAIN_PREFLIGHT_PARAM, "false");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/api/v1/channel/*");
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/assets/*");

    }

    @Override
    public void run(Config config, Environment environment) throws IOException {
        this.config = config;

        final boolean async = config.appConfig.isLoadDataAsync();
        final long maxConversationTimeSeconds = config.appConfig.getMaxConversationTimeInterval().toSeconds();

        // Set JSON formatting
        ObjectMapper objectMapper = environment.getObjectMapper();
        if(config.appConfig.isPrettyPrintJsonResponse()) {
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        }
        objectMapper.registerModule(new GraphModule());
        objectMapper.registerModule(new JodaModule());
        objectMapper.registerModule(new HistogramModule());
        objectMapper.registerModule(new TupleModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        final String appMode = config.appConfig.getMode();
        logger.warn("Server running in '{}' mode.", appMode);

        config.setObjectMapper(objectMapper);

//        // Auth
//        final String realm = "CHITCHAT REALM";
//        if("basiclist".equals(config.auth.type)){
//            environment.jersey().register(new AuthDynamicFeature(new BasicCredentialAuthFilter.Builder<User>()
//                    .setAuthenticator(new BasicAuthenticator(config.auth.credentials))
//                    .setAuthorizer(new SimpleAuthorizer())
//                    .setRealm(realm)
//                    .buildAuthFilter()));
//            environment.jersey().register(RolesAllowedDynamicFeature.class);
//            //If you want to use @Auth to inject a custom Principal type into your resource
//            environment.jersey().register(new AuthValueFactoryProvider.Binder<>(User.class));
//        } else if("oauth".equals(config.auth.type)){
//            environment.jersey().register(new AuthDynamicFeature(
//                    new OAuthCredentialAuthFilter.Builder<User>()
//                            .setAuthenticator(new TokenAuthenticator())
//                            .setAuthorizer(new SimpleAuthorizer())
//                            .setPrefix("Bearer")
//                            .setRealm(realm)
//                            .buildAuthFilter()));
//            environment.jersey().register(RolesAllowedDynamicFeature.class);
//            //If you want to use @Auth to inject a custom Principal type into your resource
//            environment.jersey().register(new AuthValueFactoryProvider.Binder<>(User.class));
//        } else if ("keycloak".equals(config.auth.type)) {
//            ConstraintSecurityHandler securityHandler = new ConstraintSecurityHandler();
//            environment.getApplicationContext().setSecurityHandler(securityHandler);
//            securityHandler.addRole("user");
//            ConstraintMapping constraintMapping = new ConstraintMapping();
//            constraintMapping.setPathSpec("/*");
//            Constraint constraint = new Constraint();
//            constraint.setAuthenticate(true);
//            constraint.setRoles(new String[]{"USER", "ADMIN"});
//            constraintMapping.setConstraint(constraint);
//            securityHandler.addConstraintMapping(constraintMapping);
//
//            KeycloakJettyAuthenticator keycloak = new KeycloakJettyAuthenticator();
//            environment.getApplicationContext().getSecurityHandler().setAuthenticator(keycloak);
//            keycloak.setAdapterConfig(config.getKeycloakConfiguration());
//
//            // allow (stateful) sessions in Dropwizard, needed for Keycloak
//            environment.jersey().register(HttpSessionFactory.class);
//            environment.servlets().setSessionHandler(new SessionHandler());
//
//            // support annotation @RolesAllowed
//            environment.jersey().register(RolesAllowedDynamicFeature.class);
//
//        }

        // Enable Cross-Origin Resource Sharing
        configureCors(environment);

        // Exceptions
        environment.jersey().register(ScriptExceptionMapper.class);
        environment.jersey().register(MultiScriptExceptionMapper.class);

        // Load data
        config.loadNlp(async);

        // DB
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, config.getDataSourceFactory(), "database");
        jdbi.registerRowMapper(Message.class, new MessageMapper());
        jdbi.registerRowMapper(Conversation.class, new ConversationMapper());
        jdbi.registerRowMapper(Range.class, new RangeMapper());
        jdbi.registerRowMapper(TextDoc.class, new TextDocMapper());
        jdbi.registerRowMapper(JsonDoc.class, new JsonDocMapper());
        jdbi.registerRowMapper(Ace.class, new AceMapper());

        // create DAOs
        config.dao = new Dao(
                jdbi.onDemand(BaseDao.class),
                jdbi.onDemand(ConversationDao.class),
                jdbi.onDemand(TextDocDao.class),
                jdbi.onDemand(JsonDocDao.class),
                jdbi.onDemand(AceDao.class)
            );


        // Resources
        if("demo".equals(appMode)){
            // restricted app server
            final SystemResource systemResource = new SystemResource(config);
            environment.jersey().register(systemResource);

            final NlpResource nlpResource = new NlpResource(config);
            environment.jersey().register(nlpResource);

            final SnaResource snaResource = new SnaResource(config);
            environment.jersey().register(snaResource);

            final ImageResource imageResource = new ImageResource(config);
            environment.jersey().register(imageResource);

            config.databaseResource = new org.c4i.chitchat.api.resource.demo.DatabaseResource(config);
            environment.lifecycle().manage(config.databaseResource);
            environment.jersey().register(config.databaseResource);

            final ScriptResource scriptResource = new ScriptResource(config);
            environment.jersey().register(scriptResource);

            // Data sheets
            config.databaseResource.loadAllDataSheets(async);

            // Reply variables
            config.databaseResource.loadReplyVariables();


            ConversationManager devChairman = new ConversationManager(10000, maxConversationTimeSeconds, TimeUnit.SECONDS, ImmutableList.of(config.databaseResource));
            final DevBotResource devBotResource = new DevBotResource(config, devChairman,ImmutableList.of());
            config.devBotResource = devBotResource;
            environment.jersey().register(devBotResource);

            ConversationManager devQAChairman = new ConversationManager(10000, maxConversationTimeSeconds, TimeUnit.SECONDS, ImmutableList.of(config.databaseResource));
            final DevQAResource devQAResource = new DevQAResource(config, devQAChairman, ImmutableList.of());
            config.devQAResource = devQAResource;
            environment.jersey().register(devQAResource);

            ChitChatMultiTenantResource chitChatMtResource = new ChitChatMultiTenantResource(config, ImmutableList.of());
            config.chitChatMultiTenantResource = chitChatMtResource;
            environment.jersey().register(chitChatMtResource);

            final org.c4i.chitchat.api.resource.demo.FrontEndResource frontEndResource = new org.c4i.chitchat.api.resource.demo.FrontEndResource(config);
            environment.jersey().register(frontEndResource);

        } else {
            // full app server
            final SystemResource systemResource = new SystemResource(config);
            environment.jersey().register(systemResource);

            final NlpResource nlpResource = new NlpResource(config);
            environment.jersey().register(nlpResource);

            final SnaResource snaResource = new SnaResource(config);
            environment.jersey().register(snaResource);

            final ImageResource imageResource = new ImageResource(config);
            environment.jersey().register(imageResource);

            config.databaseResource = new DatabaseResource(config);
            environment.lifecycle().manage(config.databaseResource);
            environment.jersey().register(config.databaseResource);

            final ScriptResource scriptResource = new ScriptResource(config);
            environment.jersey().register(scriptResource);

            final AuthResource authResource = new AuthResource();
            environment.jersey().register(authResource);

            // Data sheets
            config.databaseResource.loadAllDataSheets(async);

            // Reply variables
            config.databaseResource.loadReplyVariables();

            // Facebook
            if (config.facebookSettings != null) {
                ConversationManager fbChairman = new ConversationManager(10000, maxConversationTimeSeconds, TimeUnit.SECONDS, ImmutableList.of(config.databaseResource));
                final FacebookResource facebookResource = new FacebookResource(config, fbChairman, ImmutableList.of(config.databaseResource, new MessageLogger(config)));
                try {
                    facebookResource.loadLiveScript();
                } catch (Exception e) {
                    logger.error("Could not load Facebook live script.");
                }
                config.facebookResource = facebookResource;
                environment.jersey().register(facebookResource);
                facebookResource.recoverConversations();
            } else {
                logger.warn("No Facebook settings provided: skipping channel.");
            }

            // Telegram
            if (config.telegramSettings != null) {
                ConversationManager telegramChairman = new ConversationManager(10000, maxConversationTimeSeconds, TimeUnit.SECONDS, ImmutableList.of(config.databaseResource));
                final TelegramResource telegramResource = new TelegramResource(config, telegramChairman, ImmutableList.of(config.databaseResource, new MessageLogger(config)));
                try {
                    telegramResource.loadLiveScript();
                } catch (Exception e) {
                    logger.error("Could not load Telegram live script.");
                }
                config.telegramResource = telegramResource;
                environment.jersey().register(telegramResource);
                telegramResource.recoverConversations();
            } else {
                logger.warn("No Telegram settings provided: skipping channel.");
            }

            // Twilio
            if (config.twilioSettings != null) {
                final TwilioResource twilioResource = new TwilioResource(config, ImmutableList.of(config.databaseResource, new MessageLogger(config)));

                config.twilioResource = twilioResource;
                environment.jersey().register(twilioResource);

                environment.servlets().addFilter("TwilioRequestValidatorFilter", new TwilioRequestValidatorFilter(config.twilioSettings))
                        .addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/api/v1/channel/twilio/*");
            } else {
                logger.warn("No Twilio settings provided: skipping channel.");
            }

            ConversationManager devChairman = new ConversationManager(10000, maxConversationTimeSeconds, TimeUnit.SECONDS, ImmutableList.of(config.databaseResource));
            final DevBotResource devBotResource = new DevBotResource(config, devChairman,
                    ImmutableList.of(config.databaseResource, new MessageLogger(config), new SimpleMessageLogger()));
            config.devBotResource = devBotResource;
            environment.jersey().register(devBotResource);

            ConversationManager devQAChairman = new ConversationManager(10000, maxConversationTimeSeconds, TimeUnit.SECONDS, ImmutableList.of(config.databaseResource));
            final DevQAResource devQAResource = new DevQAResource(config, devQAChairman,
                    ImmutableList.of(config.databaseResource, new MessageLogger(config), new SimpleMessageLogger()));
            config.devQAResource = devQAResource;
            environment.jersey().register(devQAResource);

            ConversationManager ccChairman = new ConversationManager(10000, maxConversationTimeSeconds, TimeUnit.SECONDS, ImmutableList.of(config.databaseResource));
            final ChitChatResource chitChatResource = new ChitChatResource(config, ccChairman,
                    ImmutableList.of(config.databaseResource, new MessageLogger(config), new SimpleMessageLogger()));
            try {
                chitChatResource.loadLiveScript();
            } catch (Exception e) {
                logger.error("Could not load ChitChat live script.");
            }
            config.chitchatResource = chitChatResource;
            environment.jersey().register(chitChatResource);
            config.chitchatResource.recoverConversations();


            ChitChatMultiTenantResource chitChatMtResource = new ChitChatMultiTenantResource(config, ImmutableList.of(config.databaseResource, new MessageLogger(config), new SimpleMessageLogger()));
            config.chitChatMultiTenantResource = chitChatMtResource;
            environment.jersey().register(chitChatMtResource);
            chitChatMtResource.recoverConversations();

            final FrontEndResource frontEndResource = new FrontEndResource(config);
            environment.jersey().register(frontEndResource);

            // Custom tasks
            config.startPeru();

            //final LiveResource liveResource = new LiveResource(config);
            //config.liveResource = liveResource;
            //environment.jersey().register(liveResource);
            ExecutorService executor = Executors.newSingleThreadScheduledExecutor();
            executor.execute(config::startPeru);
            executor.execute(config::startHonduras);
            executor.shutdown();

        }

        // Health checks
        environment.healthChecks().register("Data directory", new DataDirHealth(config));
        environment.healthChecks().register("Pg Crypto", new PgExtensionCryptHealth(config));
        environment.healthChecks().register("Pg UUID", new PgExtensionUuidHealth(config));
        environment.healthChecks().register("Facebook channel", new FacebookHealth(config));
        environment.healthChecks().register("Telegram channel", new TelegramHealth(config));

        logger.warn("ChitChat application ready...");

    }


}