package org.c4i.chitchat.api.view;

import org.c4i.chitchat.api.Config;

public class PageParams {
    private final String contentFtl;
    protected final Config config;

    public PageParams(String contentFtl, Config config) {
        this.contentFtl = contentFtl;
        this.config = config;
    }

    public String getContentFtl() {
        return contentFtl;
    }
    public String getKeycloakServer() {return config.getKeycloakConfiguration().getAuthServerUrl();}
    public String getKeycloakRealm() {return config.getKeycloakConfiguration().getRealm();}
    public String getKeycloakClient() {return config.getKeycloakConfiguration().getResource();}
}
