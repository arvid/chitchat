package org.c4i.chitchat.api.resource.demo;

import com.google.common.collect.ImmutableSet;
import io.swagger.annotations.Api;
import org.c4i.chitchat.api.Config;
import org.c4i.chitchat.api.view.PageParams;
import org.c4i.chitchat.api.view.PageView;
import org.c4i.chitchat.api.view.SimpleView;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Set;

/**
 * User interface server side rendering
 * @version 24-10-16
 * @author Arvid Halma
 */

@Path("/ui")
@Api("/ui")
@Consumes({MediaType.APPLICATION_JSON})
@Produces(MediaType.TEXT_HTML)
public class FrontEndResource {

    private final Set<String> notInDemoMode = ImmutableSet.of(
            "data-export.ftl",
            "analyse-overview.ftl",
            "analyse-trend.ftl",
            "analyse-graph.ftl",
            "live-fb.ftl",
            "live-chitchat.ftl",
            "live-telegram.ftl",
            "system.ftl",
            "system-performance.ftl"
    );

    private Config config;

    public FrontEndResource(Config config) {
        this.config = config;
    };

    @GET
    public PageView base(){
        throw redirect("/api/v1/ui/dashboard");
    }

    private PageView pageView(String contentFtl){
        if(notInDemoMode.contains(contentFtl)){
            return new PageView(new PageParams("demo-not-allowed.ftl", config));
        } else {
            return new PageView(new PageParams(contentFtl, config));
        }
    }

    @GET
    @Path("chatclient")
    public SimpleView chatclient(@QueryParam("script") @DefaultValue("") String script){
        return script.toLowerCase().contains("cmam") ?
                new SimpleView("chatclient-sn.ftl") :
                new SimpleView("chatclient.ftl");
    }

    @GET
    @Path("dashboard")
    public PageView dashboard(){
        return pageView("dashboard.ftl");
    }


    @GET
    @Path("tutorial")
    public PageView tutorial(){
        return pageView("tutorial.ftl");
    }

    @GET
    @Path("create/survey")
    public PageView scriptBuilder(){
        return pageView("create-survey.ftl");
    }

    @GET
    @Path("create/match")
    public PageView scriptMatch(){
        return pageView("create-match.ftl");
    }

    @GET
    @Path("create/script")
    public PageView scriptChat(){
        return pageView("create-script.ftl");
    }

    @GET
    @Path("create/qa")
    public PageView scriptLevi(){
        return pageView("create-qa.ftl");
    }

    @GET
    @Path("data/variables")
    @RolesAllowed("ADMIN")
    public PageView variables() {
        return pageView("data-variables.ftl");
    }

    @GET
    @Path("data/sheets")
    @RolesAllowed("ADMIN")
    public PageView datasheets() {
        return pageView("data-sheets.ftl");
    }

    @GET
    @Path("data/export")
    public PageView export() {
        return pageView("data-export.ftl");
    }

    @GET
    @Path("analyse/overview")
    public PageView overview() {
        return pageView("analyse-overview.ftl");
    }

   /* @GET
    @Path("analyse/conversations")
    public PageView conversations(){
        return pageView("analyse-conversations.ftl");
    }

    @GET
    @Path("analyse/statistics")
    @RolesAllowed("ADMIN")
    public PageView statistics(){
        return pageView("analyse-analyse.ftl");
    }*/

    @GET
    @Path("analyse/trend")
    public PageView trend(){
        return pageView("analyse-trend.ftl");
    }

    @GET
    @Path("analyse/graph")
    public PageView graph(){
        return pageView("analyse-graph.ftl");
    }


    @GET
    @Path("live/fb")
    public PageView liveFacebook() {
        return pageView("live-fb.ftl");
    }

    @GET
    @Path("live/chitchat")
    public PageView liveChitchat() {
        return pageView("live-chitchat.ftl");
    }

    @GET
    @Path("live/telegram")
    public PageView liveTelegram() {
        return pageView("live-telegram.ftl");
    }

    @GET
    @Path("system/docs")
    public PageView docs(){
        return pageView("system-docs.ftl");
    }

    @GET
    @Path("system/apidoc")
    @RolesAllowed("ADMIN")
    public PageView apidoc(){
        return pageView("system-apidoc.ftl");
    }

    @GET
    @Path("system/performance")
    public PageView performance() {
        return pageView("system-performance.ftl");
    }

    @GET
    @Path("system/info")
    public PageView system() {
        return pageView("system.ftl");
    }

    @GET
    @Path("system/speech")
    public PageView systemSpeech() {
        return pageView("system-speech.ftl");
    }

    @GET
    @Path("about")
    public PageView about() {
        return pageView("about.ftl");
    }

    @GET
    @Path("privacy")
    public PageView privacy() {
        return pageView("privacy.ftl");
    }

    private static WebApplicationException redirect(String path) {
        URI uri = UriBuilder.fromUri(path).build();
        Response response = Response.seeOther(uri).build();
        return new WebApplicationException(response);
    }

}
