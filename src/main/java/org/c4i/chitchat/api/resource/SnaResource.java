package org.c4i.chitchat.api.resource;


import com.codahale.metrics.annotation.Timed;
import dk.aaue.sna.alg.centrality.BrandesBetweennessCentrality;
import dk.aaue.sna.alg.centrality.CentralityResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.c4i.chitchat.api.Config;
import org.c4i.chitchat.api.model.TextDoc;
import org.c4i.graph.PropVertex;
import org.c4i.graph.PropWeightedEdge;
import org.c4i.nlp.Nlp;
import org.c4i.nlp.match.Compiler;
import org.c4i.nlp.match.*;
import org.c4i.util.Histogram;
import org.c4i.util.Tuple;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jgrapht.WeightedGraph;
import org.jgrapht.graph.ListenableUndirectedWeightedGraph;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.c4i.util.Tuple.tuple;

/**
 * Social network analysis
 * @author Arvid Halma
 */

@Path("/sna")
@Api("/sna")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class SnaResource {

    private Config config;
    private final Logger logger = LoggerFactory.getLogger(SnaResource.class);


    public SnaResource(Config configuration) {
        this.config = configuration;
    }


    boolean globalCancel = false;
    double globalProgress = 0;

    @POST
    @Timed
    @Path("/graph/labelconv")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Analyse connections between labels and conversations",
            response = WeightedGraph.class
    )
    public Tuple.Tuple2<Result, WeightedGraph<PropVertex, PropWeightedEdge>> graphLabelConversations(
            @FormDataParam("channel") String channel,
            @FormDataParam("from") String from,
            @FormDataParam("to") String to
    ) {
        DateTime fromDate = ISODateTimeFormat.dateTimeParser().parseDateTime(from);
        DateTime toDate = ISODateTimeFormat.dateTimeParser().parseDateTime(to);

        final WeightedGraph<PropVertex, PropWeightedEdge> g = new ListenableUndirectedWeightedGraph<>(PropWeightedEdge.class);

        List<Range> ranges = config.dao.conversationDao.conversationRanges(channel, fromDate, toDate);
        Map<String, List<Range>> convToRanges = new HashMap<>();
        for (Range range : ranges) {
            final String cid = range.conversationId;
            convToRanges.computeIfAbsent(cid, key -> new ArrayList<>());
            convToRanges.get(cid).add(range);
        }

        int i = 1;
        for (String conv : convToRanges.keySet()) {

            PropVertex c = new PropVertex("c"+(i++));
            c.put("type", "conv");
            c.put("convId", conv);

            // ensure node
            if (!g.containsVertex(c)) {
                g.addVertex(c);
            }


            for (Range range : convToRanges.get(conv)) {
                PropVertex b = new PropVertex(range.label);
                b.put("type", "match");

                // ensure node
                if (!g.containsVertex(b)) {
                    g.addVertex(b);
                }

                PropWeightedEdge e = g.addEdge(c, b);
                if (e != null) {
//                    Map<String, Object> props = e.getProps();
//                    props.put("type", "match");
//                    props.put("convId", range.conversationId);
//                    props.put("value", range.value);
//                    props.putAll(range.props);
                }

            }
        }
        // calculate centrality
        betweenness(g);
        return tuple(null, g);
    }


    @POST
    @Timed
    @Path("/graph/scriptstruct")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Analyse labels hierarchy in conversations for a given script",
            response = WeightedGraph.class
    )
    public Tuple.Tuple2<Result, WeightedGraph<PropVertex, PropWeightedEdge>> graphScriptStructure(
            @FormDataParam("channel") String channel,
            @FormDataParam("from") String from,
            @FormDataParam("to") String to
    ) {
        DateTime fromDate = ISODateTimeFormat.dateTimeParser().parseDateTime(from);
        DateTime toDate = ISODateTimeFormat.dateTimeParser().parseDateTime(to);

        final WeightedGraph<PropVertex, PropWeightedEdge> g = new ListenableUndirectedWeightedGraph<>(PropWeightedEdge.class);

        String scriptName;
        if("fb".equals(channel)){
            scriptName = "Facebook live script";
        } else if("telegram".equals(channel)){
            scriptName = "Telegram live script";
        } else if("chitchat".equals(channel)){
            scriptName = "ChitChat live script";
        } else if(channel.startsWith("chitchat-")){
            scriptName = channel.substring(9);
        } else {
            throw new WebApplicationException("Unsupported channel: " + channel);
        }


        logger.info("Loading Graph Analysis script: {}", scriptName);
        TextDoc src = config.dao.textDocDao.getLastUpdatedByName("ccs", scriptName);

        final Nlp nlp = config.getNlp();
        Script script = Compiler.compile(src.getBody(), nlp);

        /*
        E.g. script:
        @foo <- apple | @bar | @baz
        @bar <- @baz
        @baz <- qux

        Result:
        script.dependencyGraph()    = {bar=[foo <- bar], baz=[foo <- baz, bar <- baz]}
        script.dependencyGraphInv() = {bar=[baz <- bar], foo=[bar <- foo, baz <- foo], baz=[]}
        script.dependencyTree()     = [foo:[baz:[], bar:[baz:[]]], bar:[baz:[]], baz:[]]
         */
        Map<String, List<Script.Dependency>> dependencyGraphInv = script.dependencyGraphInv();

        List<Range> ranges = config.dao.conversationDao.conversationRanges(channel, fromDate, toDate);
        for (Range range : ranges) {
            String aLabel = range.label;

            PropVertex a = new PropVertex(aLabel);

            // ensure node
            if(!g.containsVertex(a)){
                g.addVertex(a);
            }

            List<Script.Dependency> dependencies = dependencyGraphInv.getOrDefault(aLabel, ImmutableList.of());
            for (Script.Dependency dependency : dependencies) {
                PropVertex b = new PropVertex(dependency.to);

                // ensure node
                if(!g.containsVertex(b)){
                    g.addVertex(b);
                }

                PropWeightedEdge e = g.addEdge(a, b);
                if(e != null){
                    Map<String, Object> props = e.getProps();
                    props.put("convId", range.conversationId);
                    props.put("value", range.value);
                    props.putAll(range.props);
                }
            }
        }
        // calculate centrality
        betweenness(g);
        return tuple(null, g);
    }

    @POST
    @Path("/cancel/global")
    public void globalCancel() {
        globalCancel = true;
    }

    @GET
    @Path("/progress/global")
    public double globalProgress() {
        return globalProgress;
    }

    @POST
    @Timed
    @Path("/graph/actor")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(
            value = "Analyse connection in texts",
            response = WeightedGraph.class
    )
    public Tuple.Tuple2<Result, WeightedGraph<PropVertex, PropWeightedEdge>> graphActor(
            @FormDataParam("script") String script,
            @FormDataParam("channel") String channel,
            @FormDataParam("limit") @DefaultValue("0") int limit
    ) {
        globalCancel = false;
        globalProgress = 0;
        final WeightedGraph<PropVertex, PropWeightedEdge> g2 = new ListenableUndirectedWeightedGraph<>(PropWeightedEdge.class);

        final Nlp nlp = config.getNlp();
        Script scr = Compiler.compile(script, nlp);

        Result aggResult = new Result();
        aggResult.setWordFrequencies(new HashMap<>());

        if(limit <= 0){
            limit = Integer.MAX_VALUE;
        }

        List<TextDoc> docs = config.dao.textDocDao.getAllLastUpdated(channel, limit);
        Tuple.Tuple2<Result, WeightedGraph<PropVertex, PropWeightedEdge>> tuple;
        int nDocs = docs.size();
        for (int i = 0; i < nDocs; i++) {

            if(globalCancel){
                break;
            }
            TextDoc doc = docs.get(i);
            String body = doc.getBody();
            String docName = doc.getName();
            logger.info("Analysing {} [{}/{}]", docName, (i+1), nDocs);

            tuple = new SNA(config.getNlp()).findGraphActor(scr, docName, body);

            final Map<String, Histogram<String>> wordFrequencies = tuple.a.getWordFrequencies();
            wordFrequencies.forEach(
                    (key, value) -> aggResult.getWordFrequencies().merge(key, value, Histogram::join));

            WeightedGraph<PropVertex, PropWeightedEdge> g = tuple.b;


            for (PropVertex v : g.vertexSet()) {
                if (!g2.containsVertex(v)) {
                    g2.addVertex(v);
                }
            }

            for (PropWeightedEdge e : g.edgeSet()) {
                if (!g2.containsEdge(e)) {
                    PropVertex source = (PropVertex) e.getSource();
                    PropVertex target = (PropVertex) e.getTarget();
                    PropWeightedEdge propWeightedEdge = g2.addEdge(source, target);
                    if (propWeightedEdge != null) { // todo: why null?
                        propWeightedEdge.setProps(e.getProps());
                    }
                }
            }
            globalProgress = ((double)i)/nDocs;
        }

        // calculate centrality
        betweenness(g2);

//        aggResult.getWordFrequencies().values().forEach(h);

        return tuple(aggResult, g2);
    }

    private WeightedGraph<PropVertex, PropWeightedEdge> betweenness(WeightedGraph<PropVertex, PropWeightedEdge> g2) {
        BrandesBetweennessCentrality<PropVertex, PropWeightedEdge> centralityMeasure = new BrandesBetweennessCentrality<>(g2);

        CentralityResult<PropVertex> centralityResult = centralityMeasure.calculate();
        Map<PropVertex, Double> edgeWeights = centralityResult.getRaw();
        edgeWeights.forEach((vertex, value) -> vertex.put("betweenness", value));

        return g2;
    }
}