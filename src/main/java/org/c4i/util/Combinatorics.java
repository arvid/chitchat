package org.c4i.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Set and list operations.
 * @author Arvid Halma
 * @version 19-8-19
 */
public class Combinatorics {

    /**
     * List of all sub lists.
     * [1, 2, 3] ->  [[1], [1, 2], [1, 2, 3], [2], [2, 3], [3]]
     * @param as original list
     * @param <A> element type
     * @return list of all (non-emtpy) sub lists.
     */
    public static <A> List<List<A>> subLists(List<A> as){
        List<List<A>> result = new ArrayList<>();
        final int n = as.size();
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n + 1; j++) {
                result.add(as.subList(i, j));
            }
        }
        return result;
    }
}
