package org.c4i.util.time;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

/**
 * TimeLine to JSON converter.
 * @author Arvid Halma
 * @version 16-2-17
 */
public class TimelineSerializer extends StdSerializer<Timeline> {

    public TimelineSerializer() {
        this(null);
    }

    public TimelineSerializer(Class<Timeline> t) {
        super(t);
    }

    @Override
    public void serialize(
            Timeline timeline, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {

        jgen.writeStartArray();
        for (Object value : timeline) {
            jgen.writeObject(value);
        }
        jgen.writeEndArray();
    }
}