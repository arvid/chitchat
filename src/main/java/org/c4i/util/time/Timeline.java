package org.c4i.util.time;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Period;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.DoubleBinaryOperator;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

/**
 * A sequence/series of elements, that can be sliced, sampled and analyzed.
 *
 * @version 30-1-17
 * @author Arvid Halma
 */
@SuppressWarnings({"WeakerAccess", "unused"})
@JsonSerialize(using = TimelineSerializer.class)
public class Timeline<E> implements Iterable<E>, Collection<E>{
    private NavigableMap<DateTime, List<E>> events;

    /**
     * Create empty timeline
     */
    public Timeline() {
        this.events = new TreeMap<>();
    }

    /**
     * Create timeline from map
     * @param dateTimeESortedMap
     */
    protected Timeline(Map<DateTime, List<E>> dateTimeESortedMap) {
        this.events = new TreeMap<>(dateTimeESortedMap);
    }

    /**
     * Shallow copy the given timeline
     * @param timeline
     */
    protected Timeline(Timeline<E> timeline) {
        this.events = new TreeMap<>(timeline.events);
    }

    /**
     * Returns the values to which the specified time is mapped, or null if this timeline contains no mapping for this time.
     * @param t the probing time
     * @return values to which the specified key is mapped, or null if this map contains no mapping for the key
     */
    public List<E> get(DateTime t){
        return events.get(t);
    }

    /**
     * Returns a time-value pair associated the greatest key less than or equal to the given time, or null if there is no such time.
     * @param t the probing time
     * @return an entry with the greatest key less than or equal to key, or null if there is no such key
     */
    public Map.Entry<DateTime, List<E>> getFloorEntry(DateTime t){
        return events.floorEntry(t);
    }

    /**
     * Returns a time-value pair associated the greatest key less than or equal to the given time, or null if there is no such time.
     * @param t the probing time
     * @return values with the greatest key less than or equal to key, or null if there is no such key
     */
    public List<E> getFloor(DateTime t){
        Map.Entry<DateTime, List<E>> entry = events.floorEntry(t);
        return entry == null ? null : entry.getValue();
    }

    /**
     * Returns a time-value pair associated with the least time greater than or equal to the given time, or null if there is no such time.
     * @param t the probing time
     * @return an entry with the least key greater than or equal to key, or null if there is no such key
     */
    public Map.Entry<DateTime, List<E>> getCeilEntry(DateTime t){
        return events.ceilingEntry(t);
    }

    /**
     * Returns a value associated with the least time greater than or equal to the given time, or null if there is no such time.
     * @param t the probing time
     * @return values with the least key greater than or equal to key, or null if there is no such key
     */
    public List<E> getCeil(DateTime t){
        Map.Entry<DateTime, List<E>> entry = events.ceilingEntry(t);
        return entry == null ? null : entry.getValue();
    }

    /**
     * Retrieves floor or ceil entry (whichever is closest to the given t)
     * @param t the probing time
     * @return the floor or ceil entry
     */
    public Map.Entry<DateTime, List<E>> getClosestEntry(DateTime t){
        Map.Entry<DateTime, List<E>> floor = events.floorEntry(t);
        Map.Entry<DateTime, List<E>> ceil = events.ceilingEntry(t);
        if(floor == null){
            return ceil;
        } else if (ceil == null){
            return floor;
        } else {
            long millis = t.getMillis();
            return millis - floor.getKey().getMillis() < ceil.getKey().getMillis() - millis ? floor : ceil;
        }
    }

    public List<E> getClosest(DateTime t){
        Map.Entry<DateTime, List<E>> closestEntry = getClosestEntry(t);
        return closestEntry == null ? null : closestEntry.getValue();
    }

    /**
     * Get aggregated score for the given timestamp
     * @param t the probing time
     * @param toDoubleFunction the way of extracting a double value from an element E
     * @param reduceOp the way of aggregating doubles
     * @param defaultValue the value returned when there is no entry
     * @return the aggregate or else the defaultValue
     */
    public double getValue(DateTime t, ToDoubleFunction<E> toDoubleFunction, DoubleBinaryOperator reduceOp, double defaultValue){
        return events.get(t).stream().mapToDouble(toDoubleFunction).reduce(reduceOp).orElse(defaultValue);
    }

    /**
     * Weighted average between the aggregate of surrounding elements
     * @param t the probing time
     * @param toDoubleFunction the way of extracting a double value from an element E
     * @return the aggregate or else the defaultValue
     */
    public double interpolatedValue(DateTime t, ToDoubleFunction<E> toDoubleFunction){
        return interpolatedValue(t, toDoubleFunction, StatReduce.MEAN);
    }

    /**
     * Weighted average between the aggregate of surrounding elements
     * @param t the probing time
     * @param toDoubleFunction the way of extracting a double value from an element E
     * @param reduce the way of aggregating doubles
     * @return the aggregate or else the defaultValue
     */
    public double interpolatedValue(DateTime t, ToDoubleFunction<E> toDoubleFunction, StatReduce reduce){
        Map.Entry<DateTime, List<E>> e0 = events.floorEntry(t);
        Map.Entry<DateTime, List<E>> e1 = events.ceilingEntry(t);

        if(e0 == null){
            // before first event
            return e1.getValue() == null ? 0 : reduce.apply(e1.getValue().stream().mapToDouble(toDoubleFunction));
        } else if (e1 == null){
            // after last event
            return e0.getValue() == null ? 0 : reduce.apply(e0.getValue().stream().mapToDouble(toDoubleFunction));
        } else {
            double y0 = reduce.apply(e0.getValue().stream().mapToDouble(toDoubleFunction));
            double y1 = reduce.apply(e1.getValue().stream().mapToDouble(toDoubleFunction));
            long tStart = e0.getKey().getMillis();
            double dt = e1.getKey().getMillis() - tStart;
            double w = t.getMillis() - tStart;
            return dt == 0.0 ?  0.5*(y0+y1) : y0 + (w/dt) * (y1-y0);
        }
    }

    public double interpolatedValue(DateTime t, ToDoubleFunction<E> toDoubleFunction, StatReduce reduce, Period maxDt, double elseValue){
        Map.Entry<DateTime, List<E>> e0 = events.floorEntry(t);
        Map.Entry<DateTime, List<E>> e1 = events.ceilingEntry(t);

        long maxDtMillis = maxDt.toStandardDuration().getMillis();
        if(e0 == null){
            // before first event
            if (e1.getValue() == null) {
                return elseValue;
            } else {
                long dt = e1.getKey().getMillis() - t.getMillis();
                return maxDtMillis < dt ? elseValue : reduce.apply(e1.getValue().stream().mapToDouble(toDoubleFunction));
            }
        } else if (e1 == null){
            // after last event
            if (e0.getValue() == null) {
                return elseValue;
            } else {
                long dt = t.getMillis() - e0.getKey().getMillis();
                return maxDtMillis < dt ? elseValue : reduce.apply(e0.getValue().stream().mapToDouble(toDoubleFunction));
            }
        } else {
            double y0 = reduce.apply(e0.getValue().stream().mapToDouble(toDoubleFunction));
            double y1 = reduce.apply(e1.getValue().stream().mapToDouble(toDoubleFunction));
            long tStart = e0.getKey().getMillis();
            long tEnd = e1.getKey().getMillis();
            double dt = tEnd - tStart;

            long tMillis = t.getMillis();
            double w = tMillis - tStart;
            if(w > maxDtMillis && tEnd - w > maxDtMillis){
                return elseValue;
            }
            return dt == 0.0 ?  0.5*(y0+y1) : y0 + (w/dt) * (y1-y0);
        }
    }

    public List<E> getStart(){
        return this.events.firstEntry().getValue();
    }

    public List<E> getEnd(){
        return this.events.lastEntry().getValue();
    }

    /**
     * First timestamp recorded in this timeline
     * @return earliest datetime
     */
    public DateTime getStartDateTime(){
        return this.events.firstKey();
    }

    /**
     * Last timestamp recorded in this timeline
     * @return last datetime
     */
    public DateTime getEndDateTime(){
        return this.events.lastKey();
    }

    /**
     * Add element. If it is {@link Timestamped}, the provided {@link DateTime} is used, otherwise {@link DateTime#now()}.
     * @param event the element to add
     * @return true if this collection changed as a result of the call
     */
    public boolean add(E event){
        DateTime t = event instanceof Timestamped ? ((Timestamped)event).getTimestamp()  : DateTime.now();
        return add(t, event);
    }

    /**
     * Add element at the given timestamp.
     * @param entry the (key, value) pair to add
     * @return true if this collection changed as a result of the call
     */
    public boolean add(Map.Entry<DateTime, E> entry){
        return add(entry.getKey(), entry.getValue());
    }

    /**
     * Add element at the given timestamp.
     * @param t the timstamp
     * @param event the element to add
     * @return true if this collection changed as a result of the call
     */
    public synchronized boolean add(DateTime t, E event){
        if(!events.containsKey(t)){
            events.put(t, new ArrayList<>(1));
        }
        if(event != null){
            events.get(t).add(event);
            return true;
        } else {
            return false;
        }
    }

    public synchronized boolean addAll(DateTime t, Collection<E> events){
        if(!this.events.containsKey(t)){
            this.events.put(t, new ArrayList<>(events.size()));
        }
        this.events.get(t).addAll(events);
        return true;
    }

    /**
     * The time span from the start to the end of this timeline
     * @return the duration
     */
    public Duration getDuration(){
        return new Duration(getStartDateTime(), getEndDateTime());
    }

    /**
     * Take a slice of the time line with all elements within the given bounds.
     * @param t0 low endpoint (inclusive) of the timestamps in the returned timeline
     * @param t1 high endpoint (exclusive) of the timestamps in the returned timeline
     * @return a new timeline object
     */
    public Timeline<E> select(DateTime t0, DateTime t1){
        return select(t0, t1, false);
    }

    /**
     * Take a slice of the time line with all elements within the given bounds.
     * @param t0 low endpoint (inclusive) of the timestamps in the returned timeline
     * @param t1 high endpoint (exclusive) of the timestamps in the returned timeline
     * @param autoTrim trim the timeline to the first and last entries within the selection
     * @return a new timeline object
     */
    public Timeline<E> select(DateTime t0, DateTime t1, boolean autoTrim){
        final Timeline<E> timeline = new Timeline<>(events.subMap(t0, t1));
        if(!autoTrim){
            timeline.pad(t0, t1);
        }
        return timeline;
    }

    /**
     * Trim the timeline to the first and last entries of the timeline.
     * Empty values are removed.
     * @return the updated timeline instance
     */
    public Timeline<E> trim(){
        events.values().removeIf(List::isEmpty);
        return this;
    }

    /**
     * Add empty elements, possibly extending the first and last bounds of the timeline.
     * @return the updated timeline instance
     */
    public Timeline<E> pad(DateTime t0, DateTime t1) {
        add(t0, null);
        add(t1, null);
        return this;
    }

    /**
     * Aggregate all elements
     * @param reduce the way of aggregating doubles
     * @param toDouble the way of extracting a double value from an element E
     * @return the aggregate value
     */
    public double reduce(StatReduce reduce, ToDoubleFunction<E> toDouble){
        return reduce.apply(asDoubleStream(toDouble));
    }

    public Timeline<E> filter(Predicate<E> p){
        Timeline<E> result = new Timeline<>();
        for (Map.Entry<DateTime, List<E>> entry : events.entrySet()) {
            final List<E> filtered = entry.getValue().stream().filter(p).collect(Collectors.toList());
            if(!filtered.isEmpty()){
                result.addAll(entry.getKey(), filtered);
            }
        }
        return result;
    }

    /**
     * Count all elements stored in this timeline
     * @return the number of elements
     */
    public int count(){
        return events.values().parallelStream().mapToInt(List::size).sum();
    }

    /**
     * Count all elements stored in this timeline, that satisfy the given predicate
     * @param p the test for inclusion in the count
     * @return the number of elements
     */
    public int count(Predicate<E> p){
        return (int) events.values().parallelStream().flatMap(Collection::parallelStream).filter(p).count();
    }

    /**
     * The sum of all values of the elements
     * @param toDouble the way of extracting a double value from an element E
     * @return the sum
     */
    public double sum(ToDoubleFunction<E> toDouble){
        return asDoubleStream(toDouble).sum();
    }

    public Timeline<Double> interpolate(Period step, ToDoubleFunction<E> toDouble, StatReduce reduce){
        Timeline<Double> result = new Timeline<>();
        DateTime tEnd = getEndDateTime();

        for (DateTime t = getStartDateTime(); t.isBefore(tEnd); t = t.plus(step)){
            double value = interpolatedValue(t, toDouble, reduce);
            result.add(t, value);
        }
        return result;
    }

    public Timeline<Double> interpolate(Period step, ToDoubleFunction<E> toDouble, StatReduce reduce, Period maxDt, double elseValue){
        Timeline<Double> result = new Timeline<>();
        DateTime tEnd = getEndDateTime();
        for (DateTime t = getStartDateTime(); t.isBefore(tEnd); t = t.plus(step)){
            double value = interpolatedValue(t, toDouble, reduce, maxDt, elseValue);
            result.add(t, value);
        }
        return result;
    }

    /**
     * Group values, counting the entries.
     * @param step time delta
     * @return counts per time step
     */
    public Timeline<Double> resample(Period step){
        return resample(step, e -> 1.0, StatReduce.COUNT, 0.0);
    }

    /**
     * Group by time step
     * @param step time delta
     * @param toDouble How to select a value from an element
     * @param reduce how the values in the group are combined into a single value
     * @return combined values (e.g. sum, count) per time bracket
     */
    public Timeline<Double> resample(Period step, ToDoubleFunction<E> toDouble, StatReduce reduce, Double orElse){
        Timeline<Double> result = new Timeline<>();
        if(events.isEmpty()){
            return result;
        }
        DateTime tEnd = getEndDateTime();
        for (DateTime t = getStartDateTime(); t.isBefore(tEnd); t = t.plus(step)){
            double value = reduce.apply(toDoubleStream(t, t.plus(step), toDouble));
            value = Double.isNaN(value) ? orElse : value;
            result.add(t, value);
        }
        return result;
    }

    public Timeline<Double> countStats(Period step){
        return resample(step, e -> 1.0, StatReduce.COUNT, 0.0);
    }

    public List<Integer> hourOnDayStats(){
        ArrayList<Integer> groups = new ArrayList<>(24);
        for (int i = 0; i < 24; i++) {
            groups.add(i, 0);
        }

        for (Map.Entry<DateTime, List<E>> entry : events.entrySet()) {
//            Double value = reduce.apply(entry.getValue().stream().mapToDouble(toDouble));
            int value = entry.getValue().size();
            int ix = entry.getKey().getHourOfDay();
            groups.set(ix, groups.get(ix) + value);
        }

        return groups;
    }

    public Timeline<Double> hourStats(){
        return resample(Period.hours(1), d -> 1, StatReduce.COUNT, 0.0);
    }

    public Timeline<Double> hourStats(ToDoubleFunction<E> toDouble, StatReduce reduce){
        return resample(Period.hours(1), toDouble, reduce, Double.NaN);
    }

    public Timeline<Double> dayStats(){
        return resample(Period.days(1), d -> 1, StatReduce.COUNT, 0.0);
    }

    public Timeline<Double> dayStats(ToDoubleFunction<E> toDouble, StatReduce reduce){
        return resample(Period.days(1), toDouble, reduce, Double.NaN);
    }

    public Timeline<Double> weekStats(){
        return resample(Period.weeks(1), d -> 1, StatReduce.COUNT, 0.0);
    }

    public Timeline<Double> weekStats(ToDoubleFunction<E> toDouble, StatReduce reduce){
        return resample(Period.weeks(1), toDouble, reduce, Double.NaN);
    }

    public Timeline<Double> monthStats(){
        return resample(Period.months(1), d -> 1, StatReduce.COUNT, 0.0);
    }

    public Timeline<Double> monthStats(ToDoubleFunction<E> toDouble, StatReduce reduce){
        return resample(Period.months(1), toDouble, reduce, Double.NaN);
    }

    public Timeline<Double> yearStats(){
        return resample(Period.years(1), d -> 1, StatReduce.COUNT, 0.0);
    }

    public Timeline<Double> yearStats(ToDoubleFunction<E> toDouble, StatReduce reduce){
        return resample(Period.years(1), toDouble, reduce, Double.NaN);
    }

    public List<Double> weekDayStats(ToDoubleFunction<E> toDouble, StatReduce reduce){
        ArrayList<ArrayList<Double>> groups = new ArrayList<>(7);
        for (int i = 0; i < 7; i++) {
            groups.add(new ArrayList<>());
        }
        Period step = Period.days(1);
        DateTime tEnd = getEndDateTime();
        for (DateTime t = getStartDateTime(); t.isBefore(tEnd); t = t.plus(step)){
            double value = interpolatedValue(t, toDouble, reduce);
            groups.get(t.getDayOfWeek()).add(value);
        }

        return groups.stream().map(xs-> reduce.apply(xs.stream().mapToDouble(d->d))).collect(Collectors.toList());
    }

    public List<Double> weekDayStats(ToDoubleFunction<E> toDouble, double z, DoubleBinaryOperator reduce){
        ArrayList<Double> groups = new ArrayList<>(7);
        for (int i = 0; i < 7; i++) {
            groups.add(z);
        }
        StatReduce statReduce = StatReduce.asStatReduce(z, reduce);
        Period step = Period.days(1);
        DateTime tEnd = getEndDateTime();
        for (DateTime t = getStartDateTime(); t.isBefore(tEnd); t = t.plus(step)){
            int i = t.getDayOfWeek();
            double value = interpolatedValue(t, toDouble, statReduce);
            groups.set(i, reduce.applyAsDouble(groups.get(i), value));
        }

        return groups;
    }


    public Timeline<TimeValue> slidingWindow(Period window, Period step, ToDoubleFunction<E> toDouble, StatReduce reduce){
        Timeline<TimeValue> result = new Timeline<>();
        if(events.isEmpty()){
            return result;
        }

        DateTime tEnd = getEndDateTime().minus(window);
        for (DateTime t = getStartDateTime(); t.isBefore(tEnd); t = t.plus(step)){

            DateTime tw = t.plus(window);
            double value = reduce.apply(toDoubleStream(t, tw, toDouble));
            result.add(new TimeValue(tw, value));
        }

        return result;
    }

    public Timeline<Double> slidingWindow(Period window1, Period window2, Period step, ToDoubleFunction<E> toDouble, StatReduce reduce1, StatReduce reduce2, BinaryOperator<Double> windowValueCombiner){
        Timeline<Double> result = new Timeline<>();
        if(events.isEmpty()){
            return result;
        }

        DateTime tEnd = getEndDateTime().minus(window1).minus(window2);
        for (DateTime t = getStartDateTime(); t.isBefore(tEnd); t = t.plus(step)){
            DateTime tw1 = t.plus(window1);
            DateTime tw2 = tw1.plus(window2);
            double value1 = reduce1.apply(toDoubleStream(t, tw1, toDouble));
            double value2 = reduce2.apply(toDoubleStream(tw1, tw2, toDouble));
            result.add(tw1, windowValueCombiner.apply(value1, value2));
        }

        return result;
    }

    public Timeline<Double> derivative(ToDoubleFunction<E> toDouble, Period dt){
        final Timeline<Double> ys = interpolate(dt, toDouble, StatReduce.SUM);
        return ys.slidingWindow(dt, dt, dt, d->d, StatReduce.SUM, StatReduce.SUM, (ti, tj) -> tj - ti);
    }

    private DoubleStream toDoubleStream(DateTime t0, DateTime t1, ToDoubleFunction<E> toDouble) {
        return events.subMap(t0, t1).values().parallelStream().flatMapToDouble(list -> list.parallelStream().mapToDouble(toDouble));
    }

    @Override
    public Iterator<E> iterator() {
        return events.values().stream().flatMap(Collection::stream).iterator();
    }

    public List<E> asList(){
        return events.values().stream().flatMap(Collection::parallelStream).collect(Collectors.toList());
    }

    public List<Map.Entry<DateTime, E>> asEntryList(){
        List<Map.Entry<DateTime, E>> result = new ArrayList<>();
        for (Map.Entry<DateTime, List<E>> entry : events.entrySet()) {
            for (E value : entry.getValue()) {
                result.add(new AbstractMap.SimpleEntry<>(entry.getKey(), value));
            }
        }
        return result;
    }

    public List<TimeValue> asTimeValueList(ToDoubleFunction<E> toDouble){
        List<TimeValue> result = new ArrayList<>();
        for (Map.Entry<DateTime, List<E>> entry : events.entrySet()) {
            for (E value : entry.getValue()) {
                result.add(new TimeValue(entry.getKey(), toDouble.applyAsDouble(value)));
            }
        }
        return result;
    }

    @Override
    public int size() {
        return count();
    }

    @Override
    public boolean isEmpty() {
        return events.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return events.values().parallelStream().flatMap(Collection::parallelStream).anyMatch(e -> e.equals(o));
    }

    @Override
    public Object[] toArray() {
        return asList().toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        int i = 0;
        for (E e : this) {
            a[i++] = (T)e;
        }
        return a;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return c.stream().allMatch(this::contains);
    }


    @Override
    public boolean addAll(Collection<? extends E> c) {
        c.forEach(this::add);
        return true;
    }

    public boolean addAll(Timeline<E> timeline) {
        if(timeline == null){
            return false;
        }
        timeline.events.forEach(this::addAll);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("Can't remove objects from a timeLine. Use filter() instead.");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("Can't remove objects from a timeLine. Use filter() instead.");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Can't remove objects from a timeLine. Use filter() instead.");
    }

    @Override
    public void clear() {
        events.clear();
    }

    public DoubleStream asDoubleStream(ToDoubleFunction<E> toDouble) {
        return events.values().parallelStream().flatMapToDouble((es) -> es.stream().mapToDouble(toDouble));
    }

    public void print(){
        System.out.println("Timeline (size = " + count()+") from " + getStartDateTime() + " to " + getEndDateTime() );
        for (Map.Entry<DateTime, List<E>> entry : events.entrySet()) {
            System.out.println(" * " + entry.getKey() + " : \t" + entry.getValue());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Timeline)) return false;
        Timeline<?> timeline = (Timeline<?>) o;
        return Objects.equals(events, timeline.events);
    }

    @Override
    public int hashCode() {
        return Objects.hash(events);
    }

    @Override
    public String toString() {
        return "TimeLine{" + "events=" + events +
                '}';
    }
}
