package org.c4i.nlp.normalize;

/**
 * Wolof Stemmer.
 * Wolof (/ˈwoʊlɒf/) is a language of Senegal, the Gambia and Mauritania, and the native language of the Wolof people.
 *
 * In Wolof, verbs are unchangeable stems that cannot be conjugated.
 * To express different tenses or aspects of an action, personal pronouns are conjugated – not the verbs.
 *
 * Plural nouns
 * Any loan noun from French or English uses -bi: butik-bi, xarit-bi "the boutique, the friend"
 * Most Arabic or religious terms use -ji: jumma-ji, jigéen-ji, "the mosque, the girl"
 * Four nouns referring to persons use -ki/-ñi:' nit-ki, nit-ñi, 'the person, the people"
 * Plural nouns use -yi: jigéen-yi, butik-yi, "the girls, the boutiques"
 *
 * The only suffix to normalize is ..yi to ..bi.
 * Also normalizes 'ŋ' to 'n'.
 *
 * @see "https://en.wikipedia.org/wiki/Wolof_language"
 * @author Arvid Halma
 */
public class WolofStemmer implements StringNormalizer{


    @Override
    public String normalize(String s) {
        int n = s.length();
        if(n > 3 && s.endsWith("-yi")){
            s = s.substring(0, n-3) + "-bi";
        }
        s = s.replace('ŋ', 'n'); // replace strange n: assumption that it may be hard to type on some (virtual keyboards.
        return s;
    }
}
