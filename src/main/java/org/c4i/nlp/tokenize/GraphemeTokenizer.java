package org.c4i.nlp.tokenize;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Split all text in graphemes instead of words (say a letter followed by zero or more accents).
 * Some languages are written without spaces (e.g. Thai, Chinese, Japanese and Khmer).
 * Split in
 * @author Arvid Halma
 * @version 10-5-2015 - 20:55
 */
public class GraphemeTokenizer extends MatchingWordTokenizer {
    private static final Pattern GRAPHEME_PATTERN = Pattern.compile("\\P{M}\\p{M}*");

    public GraphemeTokenizer() {
    }

    @Override
    public List<Token> tokenize(String text){
        return tokenize(text, GRAPHEME_PATTERN);
    }

    public List<Token> tokenizeFull(String text){
        return tokenizeFull(text, GRAPHEME_PATTERN);
    }

    public String description(){
        return "grapheme";
    }
}
