package org.c4i.nlp.chat;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.c4i.chitchat.api.model.Scored;
import org.c4i.nlp.match.Result;
import org.c4i.nlp.normalize.StringNormalizer;
import org.c4i.nlp.tokenize.MatchingWordTokenizer;
import org.c4i.nlp.tokenize.Token;
import org.c4i.nlp.tokenize.TokenUtil;
import org.c4i.nlp.tokenize.Tokenizer;
import org.c4i.util.Csv;
import org.c4i.util.StringUtil;
import com.google.common.collect.ImmutableList;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * A simple bot that can be trained with example replies.
 * One way to look at it, is that it looks up its nearest neighbor, given the Levenshtein distance to the input message.
 */
public class QABot implements ChatBot{
//    private Map<String, List<String>> inputToReplies;

    private List<String> fallbackMessages;
    private List<QAEntry> entries;
    private StringNormalizer normalizer;

    public QABot(String tsv, StringNormalizer normalizer) throws IOException {
        entries = new ArrayList<>();
        fallbackMessages = new ArrayList<>();

        /*for (Map.Entry<String, List<String>> entry : inputToReplies.entrySet()) {
            List<Token> tokens = tokenizer.tokenize(entry.getKey());
            normalizer.normalizeTokens(tokens);
            this.inputToReplies.put(TokenUtil.toSentence(tokens), entry.getValue());
        }*/

        this.normalizer = normalizer;
        AtomicReference<String> lastQ = new AtomicReference<>("#FALLBACK");

        CSVFormat format = CSVFormat.TDF
                .withIgnoreSurroundingSpaces()
                .withFirstRecordAsHeader()
                .withNullString("");

        try {
            CSVParser parser = CSVParser.parse(tsv, format);
            for (CSVRecord row : parser) {
    //        new Csv().setInput(tsv).formatTsv().setCommmentLineStart("//").process(row -> {
                String q = row.get(0);
                String a = row.get(1);

                if (a == null) {
                    return;
                }

                if (q == null) {
                    q = lastQ.get();
                }

                if (q.equals("#FALLBACK")) {
                    fallbackMessages.add(a);
                } else {
                    QAEntry entry = new QAEntry();
                    entry.question = q;
                    entry.answer = a;

                    // create key
                    String key = q + " " + a.substring(0, Math.min(a.length(), 200));
                    for (int i = 2; i < row.size(); i++) {
                        key += " " + row.get(i); // add additional columns
                    }

                    entry.key = normalize(key);

                    entries.add(entry);
                }
                lastQ.set(q);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if(fallbackMessages.isEmpty()){
            fallbackMessages.add("?");
        }
    }

    /*public QABot(Map<String, List<String>> inputToReplies) {
        this.inputToReplies = inputToReplies;
        this.fallbackMessages = ImmutableList.of("?");
    }

    public QABot(Map<String, List<String>> inputToReplies, List<String> fallbackMessages) {
        this.inputToReplies = inputToReplies;
        this.fallbackMessages = fallbackMessages;
    }

    public QABot(Map<String, List<String>> inputToReplies, List<String> fallbackMessages, StringNormalizer normalizer) {
        this.inputToReplies = new HashMap<>();
        Tokenizer tokenizer = new MatchingWordTokenizer();

        for (Map.Entry<String, List<String>> entry : inputToReplies.entrySet()) {
            List<Token> tokens = tokenizer.tokenize(entry.getKey());
            normalizer.normalizeTokens(tokens);
            this.inputToReplies.put(TokenUtil.toSentence(tokens), entry.getValue());
        }
        this.fallbackMessages = fallbackMessages;
    }*/

    private String normalize(String s){
        Tokenizer tokenizer = new MatchingWordTokenizer();
        List<Token> tokens = tokenizer.tokenize(s);
        normalizer.normalizeTokens(tokens);
        return TokenUtil.toSentence(tokens);
    }

    @Override
    public Result reply(Conversation conversation){

        if(entries.isEmpty()) {
            String replyString = fallbackMessages.isEmpty() ? "?" : fallbackMessages.get((int) (Math.random() * fallbackMessages.size()));
            Message reply = Message.createReply(conversation, replyString);
            return new Result().setReplies(ImmutableList.of(reply));
        }

        Message msgIn = conversation.lastMessage();
        String q = normalize(msgIn.getText());


        List<Scored<QAEntry>> scores = new ArrayList<>();


        for (QAEntry entry: entries) {
//            double d = StringUtil.levenshteinMatchScore(msgIn.getText(), entry.getKey());
            double d = StringUtil.ngramCosineSimilarityWords(q, entry.key, 3);
            scores.add(new Scored<>(entry, d));
        }

        scores.sort(Collections.reverseOrder());
        double maxScore = scores.get(0).getScore();

        String replyString;
        if(maxScore == 0) { // < 0.15 == non-sense
            replyString = fallbackMessages.isEmpty() ? "?" : fallbackMessages.get((int)(Math.random()*fallbackMessages.size()));
        } else {

            int maxRelevantAnswerIx = 0;
            for (int i = 1; i < Math.min(5, scores.size()); i++) {
                double score = scores.get(i).getScore();
                if (score > maxScore * 0.97) {
                    // also good answer
                    maxRelevantAnswerIx = i;
                } else {
                    break;
                }
            }

            if (maxRelevantAnswerIx == 0) {
                // unique answer
                replyString = scores.get(0).getValue().answer;
            } else if (maxRelevantAnswerIx <= 2) {
                replyString = scores.subList(0, maxRelevantAnswerIx+1).stream()
                        .map(sqa -> {
                            final String question = sqa.getValue().question.replaceAll(",", " ");
                            return "BUTTON(" + question +", " + question + ")";
                        })
                        .collect(Collectors.joining(", or <br>",
                                "Do you mean:<br>",
                                ""));
            } else {
                replyString = fallbackMessages.get(0);
            }
        }


//        Message reply = Message.createReply(conversation, maxReplies.get((int)(maxReplies.size() * Math.random())));
        Message reply = Message.createReply(conversation, replyString);
        return new Result().setReplies(ImmutableList.of(reply));
    }

    @Override
    public String toString() {
        return "QABot{" +
                "#keys =" + entries.size() +
                ", #fallbackMessages = " + fallbackMessages.size() +
                '}';
    }

    public static class QAEntry {
        public String key;
        public String question;
        public String answer;
    }
}
