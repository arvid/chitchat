package org.c4i.nlp.match;

import org.c4i.nlp.tokenize.Token;
import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.Objects;

/**
 * Match tokens on their normalized values.
 * @author Arvid Halma
 * @version 5-2-19
 */
public class NormalizedMatchStrategy implements MatchStrategy {
    private Token a;

    public NormalizedMatchStrategy(Token pattern) {
        this.a = pattern;
    }

    @Override
    public MatchStrategy create(Token pattern) {
        return new NormalizedMatchStrategy(pattern);
    }

    @Override
    public List<String> matches(Token b) {
        return Objects.equals(a.getNormalizedWord(), b.getNormalizedWord()) ? ImmutableList.of() : null;
    }

    @Override
    public char quote() {
        return '\'';
    }
}
