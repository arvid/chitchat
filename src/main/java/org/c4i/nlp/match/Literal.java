package org.c4i.nlp.match;

import org.c4i.nlp.tokenize.MatchingWordTokenizer;
import org.c4i.nlp.tokenize.Token;
import org.c4i.util.ArrayUtil;
import org.c4i.util.StringUtil;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * A literal is an atomic formula (in this case a sequence of tokens) or its negation.
 * The definition mostly appears in proof theory (of classical logic), e.g. in conjunctive normal form.
 * It just happens to be used for that here as well.
 * <p>
 * This literal is just a container for a single tokens.
 * Evaluation is done elsewhere. In this way, serialization and expression transformations can be
 * done independently from the underlying semantics.
 *
 * @author Arvid Halma
 * @version 27-4-2016 - 20:51
 */
public class Literal implements Comparable<Literal>{
    Token[] tokens;
    boolean negated; // sign of the literal (A or NOT A)
    char meta;       // type annotation: 'a' = word-like, '@' = reference, '=' = comparison
    int marker;      // free to interpret indicator

    private static final Pattern
            MUST_QUOTE = Pattern.compile("(?U)[^@.\\w]"),
            DOUBLE_QUOTE_TO_ESCAPE = Pattern.compile("(?<!\\\\)\""),
            SINGLE_QUOTE_TO_ESCAPE = Pattern.compile("(?<!\\\\)'");


    public Literal(Token[] tokens, boolean negated, char meta) {
        this.tokens = tokens;
        this.negated = negated;
        this.meta = meta;
    }

    public Literal(Token token, boolean negated, char meta) {
        this(new Token[]{token}, negated, meta);
    }

    public Literal(Token token, char meta) {
        this(new Token[]{token}, false, meta);
    }

    public Literal(Token token, boolean negated) {
        this(new Token[]{token}, negated, 'a');
    }

    public Literal(Token token) {
        this(new Token[]{token}, false, 'a');
    }

    public static Literal createReference(String label) {
        return new Literal(new Token(label).setExactMatch(), '@');
    }

    public static Literal createExact(String text) {
        return new Literal(new Token(text).setExactMatch(), 'a');
    }

    public static Literal createInexact(String text) {
        Token[] tokens = new MatchingWordTokenizer().tokenize(text).toArray(new Token[0]);
        for (Token token : tokens) {
            token.setNormalizedMatch();
        }
        return new Literal(tokens, false, 'a');
    }


    public static Literal[][] createAnd(Literal ... lits) {
        return new Literal[][]{lits};
    }

    public static Literal[][] createOr(Literal ... lits) {
        Literal[][] result = new Literal[lits.length][];
        for (int i = 0; i < lits.length; i++) {
            result[i] = new Literal[]{lits[i]};
        }
        return result;
    }

    public Token[] getTokens() {
        return tokens;
    }

    public Literal setTokens(Token[] tokens) {
        this.tokens = tokens;
        return this;
    }

    public boolean isNegated() {
        return negated;
    }

    public Literal setNegated(boolean negated) {
        this.negated = negated;
        return this;
    }

    public char getMeta() {
        return meta;
    }

    public Literal setMeta(char meta) {
        this.meta = meta;
        return this;
    }

    public int getMarker() {
        return marker;
    }

    public Literal setMarker(int marker) {
        this.marker = marker;
        return this;
    }

    /**
     * Checks if the given {@link Literal} equals the given String exactly.
     * @param word the string matched to tokens[0]
     * @return true if tokens[0].getWord().equals(word) else false.
     */
    public boolean equals(String word){
        return tokens.length != 0 && tokens[0].getWord().equals(word);
    }

    /**
     * Checks if the given {@link Literal} matches the given regex.
     * @param pattern the string matched to tokens[0]
     * @return if matches
     */
    public boolean matches(Pattern pattern){
        return tokens.length != 0 && pattern.matcher(tokens[0].getWord()).matches();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Literal)) return false;

        Literal literal = (Literal) o;

        if (negated != literal.negated) return false;
        if (meta != literal.meta) return false;
        return Arrays.equals(tokens, literal.tokens);
    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(tokens);
        result = 31 * result + (negated ? 1 : 0);
        result = 31 * result + (int) meta;
        return result;
    }

    public static String toString(Token ... tokens){
        int nNormal = 0, nExact = 0, nWildcard = 0;
        for (Token t : tokens) {
            String word = t.getWord();
            boolean isWildcard = "?+*".contains(word);
            if(isWildcard) {
                nWildcard++;
            } else if (t.isNormalizedMatch()){
                nNormal++;
            } else {
                nExact++;
            }
        }

        if(nExact == 0 && nNormal == 1 && nWildcard == 0){
            // single normalized word
            String word = tokens[0].getWord();
            return MUST_QUOTE.matcher(word).find() ? singleQuotedString(word) : word;
        }

        if(nExact == 1 && nNormal == 0 && nWildcard == 0){
            // single exact word
            String word = tokens[0].getWord();
            return word.startsWith("@") ? word : doubleQuotedString(word);
        }

        if(nExact > 0 && nNormal == 0 && nWildcard == 0){
            // simple sequence of exact words
            if(Arrays.stream(tokens).noneMatch(t -> MUST_QUOTE.matcher(t.getWord()).find())) {
                return Arrays.stream(tokens).collect(Collectors.joining(" ", "\"", "\""));
            }
        }

        if(nExact == 0 && nNormal > 1){
            // sequence of normalized words
            if(Arrays.stream(tokens).noneMatch(t -> MUST_QUOTE.matcher(t.getWord()).find() && !"?+*".contains(t.getWord()))) {
                return Arrays.stream(tokens).collect(Collectors.joining(" ", "\'", "\'"));
            }
        }

        // mixed word types
        return Arrays.stream(tokens).map(t -> {
            String word = t.getWord();
            boolean isWildcard = "?+*".contains(word);
            if(isWildcard) {
                return word;
            } if (t.isNormalizedMatch()){
                return singleQuotedString(word);
            } else {
                return doubleQuotedString(word);
            }
        }).collect(Collectors.joining("_"));
    }

    private static String doubleQuotedString(String word){
        return '\"' + DOUBLE_QUOTE_TO_ESCAPE.matcher(word).replaceAll("\\\\\"") + '\"';
    }

    private static String singleQuotedString(String word){
        return '\'' + SINGLE_QUOTE_TO_ESCAPE.matcher(word).replaceAll("\\\\'") + '\'';
    }

    @Override
    public String toString() {
        if(meta == '='){
            String leftWord = toString(ArrayUtil.subArray(tokens, 0, marker));
            String rightWord = toString(ArrayUtil.subArray(tokens, marker+1, tokens.length));

            return leftWord+ " " + tokens[marker].getWord() + " " + rightWord;
        }

        String words = toString(tokens);
        if(meta == '@'){
            words = meta+ StringUtil.unquote(words);
        }
        if(negated){
            words = "-"+words;
        }
        return words;
    }

    @Override
    public int compareTo(Literal lit) {
        int metaCmp = Character.compare(meta, lit.meta);
        if(metaCmp != 0){
            return metaCmp;
        }

        for (int i = 0; i < tokens.length && i < lit.tokens.length ; i++) {
            Token token = tokens[i];

            int tokenComp = token.compareTo(lit.tokens[i]);
            if(tokenComp == 0) {
                if (negated && !lit.negated) {
                    return -1;
                } else if(!negated && lit.negated){
                    return 1;
                }
            } else {
                return tokenComp;
            }
        }
        // equals so far...
        return Integer.compare(tokens.length, lit.tokens.length);
    }

    /*public static void main(String[] args) {
        System.out.println(singleQuotedString("l\\'enfent"));
        System.out.println(singleQuotedString("l'enfent"));
        System.out.println(doubleQuotedString("l\\\"enfent"));
        System.out.println(doubleQuotedString("l\"enfent"));
    }*/
}
