package org.c4i.nlp.match;

import org.c4i.nlp.Nlp;

import java.util.List;

public interface ReplyPlugin {
    /**
     * Modify the reply in some way
     * @param reply the current reply string
     * @param userText the input from the user, preceding the reply
     * @param script the script used to come to this reply
     * @param ranges the matches in the userText,
     * @param nlp the language models
     * @return the updated reply string
     */
    String updateReply(String reply, String userText, Script script, List<Range> ranges, Nlp nlp);
}
