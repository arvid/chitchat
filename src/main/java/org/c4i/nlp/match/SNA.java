package org.c4i.nlp.match;

import com.google.common.collect.ImmutableList;
import org.c4i.graph.PropVertex;
import org.c4i.graph.PropWeightedEdge;
import org.c4i.nlp.Nlp;
import org.c4i.nlp.StopWords;
import org.c4i.nlp.normalize.StringNormalizer;
import org.c4i.nlp.tokenize.Token;
import org.c4i.nlp.tokenize.Tokenizer;
import org.c4i.util.ArrayUtil;
import org.c4i.util.Histogram;
import org.c4i.util.RegexUtil;
import org.c4i.util.Tuple;
import org.jgrapht.WeightedGraph;
import org.jgrapht.graph.ListenableUndirectedWeightedGraph;

import java.util.*;

import static org.c4i.util.Tuple.tuple;

/**
 * Social Network Analytics
 * @author Arvid Halma
 */
public class SNA extends Eval {


    public SNA(Nlp nlp) {
        super(nlp);
    }

    /**
     * Find instances of the patterns defined by the script in the text.
     * @param script a script.
     * @param sourceName the source (channel or document collection)
     * @param text the input text to be searched
     * @return a result containing the matched ranges.
     */
    public Tuple.Tuple2<Result, WeightedGraph<PropVertex, PropWeightedEdge>> findGraphActor(Script script, String sourceName, String text){
        normalizer = nlp.getNormalizer(script.config);
        Tokenizer tokenizer = nlp.getWordTokenizer(script.config);

        Token[][] tokens = MatchUtil.textToSentenceTokens(text, normalizer, tokenizer, nlp.getSentenceSplitter(script.config));

        Tuple.Tuple2<Result, WeightedGraph<PropVertex, PropWeightedEdge>> tuple = findGraphActor(script, sourceName, text, tokens, ImmutableList.of());

        Result result = tuple.a;
        List<Range> matches = result.getRanges();
        matches.forEach(mr -> mr.updateValue(text));

        result.removeAnonymousMatches();
//        matches = result.getRanges();
//        String highlight = Eval.highlightWithTags(text, matches);
//        result.setHighlight(highlight);
        return tuple;
    }

    /**
     * Return all matching labels
     * @param script containing label labels
     * @param sents the text split in sentences
     * @param premises presumably true label context. Those will also show in the result
     * @return match result
     */
    private Tuple.Tuple2<Result, WeightedGraph<PropVertex, PropWeightedEdge>> findGraphActor(final Script script, final String sourceName, final String text, final Token[][] sents, final List<Range> premises){
        Result result = new Result();
        final WeightedGraph<PropVertex, PropWeightedEdge> g = new ListenableUndirectedWeightedGraph<>(PropWeightedEdge.class);


        StringNormalizer normalizer = nlp.getNormalizer(script.getConfig());
        StopWords stopWords = nlp.getStopWords(script.getConfig().getLanguages().get(0));

        premises.forEach(result::addMatch);

        final Map<String, Object> scriptLabelProperties = script.getConfig().getRuleProperties().getLabel();
        final Map<String, Long> profile = new HashMap<>();
        final Map<String, Histogram<String>> histograms = new HashMap<>();


        for (int i = 0, sentsLength = sents.length; i < sentsLength; i++) {
            final int sentIx = i;

            if(sents[i].length == 0){
                continue;
            }

            script.labels.values().stream().flatMap(Collection::stream).forEach(rule -> {
                long t0 = System.nanoTime();
                final String head = rule.head;

                // slice text
                final Object within = rule.getProp("within", scriptLabelProperties);
                int lastN = 1;
                if("all".equals(within)){
                    lastN = sents.length; // all;
                } else if(within instanceof Integer){
                    lastN = (Integer)within;
                }
                Token[] scope = ArrayUtil.concatAll(Token.class, sents, sentIx + 1 - lastN, sentIx + 1);


                Range scopeRange = new Range(null
                        , scope[0].getLocation(), scope[0].getLocation()+sentsLength,
                        scope[0].getCharStart(), scope[scope.length-1].getCharEnd())
                        .setSection(scope[scope.length-1].getSection());

                // slice ranges
                Map<String, List<Range>> scopeRangeMap = new HashMap<>();
                result.getRangeStream().filter(scopeRange::contains).forEach(range -> {
                    if(!scopeRangeMap.containsKey(range.label)){
                        scopeRangeMap.put(range.label, new ArrayList<>(2));
                    }
                    scopeRangeMap.get(range.label).add(range);
                });

                List<Range> ranges = findRule(scope, rule, script, scopeRangeMap);

                // update timing info
                if(!profile.containsKey(head)){
                    profile.put(head, 0L);
                }
                profile.put(head, profile.get(head) + (System.nanoTime() - t0));

                // update result
                for (Range r : ranges) {
                    Range range = new Range(r).setLabel(head);
                    result.addMatch(range);
                    r.updateValue(text);

                    Histogram<String> localWords = new Histogram<>();
                    Arrays.stream(scope).map(Token::getNormalizedWord).filter(w -> !RegexUtil.NUMBER.matcher(w).matches() && !stopWords.isStopWord(w)).forEach(localWords::add);

                    histograms.merge(head, localWords, Histogram::join);
                }


                if("rel".equals(head)){
                    // add actors

                    int charStart = scope[0].getCharStart();
                    String highlight = highlightWithTags(text.substring(charStart, scope[scope.length-1].getCharEnd()), charStart, ranges);

                    Set<PropVertex> actors = new HashSet<>();
                    Set<PropVertex> locs = new HashSet<>();
                    Set<PropVertex> verbs = new HashSet<>();
                    for (Range r : ranges) {
                        String type = r.getProps().get("type");
                        if("actor".equals(type)){
                            PropVertex actor = new PropVertex(normalizer.normalize(r.getValue()));
                            actors.add(actor);
                            actor.getProps().put("snippet", highlight);
                            actor.getProps().put("src", sourceName);
                            actor.getProps().put("type", "actor");
                            actor.getProps().put("cat", r.getProps().get("cat"));
                            if(!g.containsVertex(actor)) {
                                g.addVertex(actor);
                            }
                        } else if("loc".equals(type)){
                            PropVertex loc = new PropVertex(normalizer.normalize(r.getValue()));
                            locs.add(loc);
                            Map<String, Object> props = loc.getProps();
                            props.put("src", sourceName);
                            props.put("type", "loc");
                            props.put("snippet", highlight);
                            props.put("lat", r.getProps().get("latitude"));
                            props.put("lon", r.getProps().get("longitude"));
                            props.put("country", r.getProps().get("country"));

                            if(!g.containsVertex(loc)) {
                                g.addVertex(loc);
                            }
                        } else if("verb".equals(type)){
                            PropVertex verb = new PropVertex(normalizer.normalize(r.getValue()));
                            verbs.add(verb);
                            Map<String, Object> props = verb.getProps();
                            props.put("src", sourceName);
                            props.put("type", "verb");
                            props.put("snippet", highlight);
                            props.put("en_term", r.getProps().get("en_term"));

                            if(!g.containsVertex(verb)) {
                                g.addVertex(verb);
                            }
                        }
                    }

                    // add combos
                    for (PropVertex verb : verbs) {
                        for (PropVertex actor : actors) {
                            try {
                                PropWeightedEdge e = g.addEdge(verb, actor);
                                g.getEdge(verb, actor).put("snippet", highlight);
                                g.getEdge(verb, actor).put("src", sourceName);
                            } catch (IllegalArgumentException ignored){
                                // java.lang.IllegalArgumentException: loops not allowed
                            }
                        }
                    }
                    for (PropVertex verb : verbs) {
                        for (PropVertex loc : locs) {
                            try {
                                PropWeightedEdge e = g.addEdge(verb, loc);
                                g.getEdge(verb, loc).put("snippet", highlight);
                                g.getEdge(verb, loc).put("src", sourceName);
                            } catch (IllegalArgumentException ignored){
                                // java.lang.IllegalArgumentException: loops not allowed
                            }
                        }
                    }
                    for (PropVertex loc : locs) {
                        for (PropVertex actor : actors) {
                            try {
                                PropWeightedEdge e = g.addEdge(loc, actor);
                                g.getEdge(loc, actor).put("snippet", highlight);
                                g.getEdge(loc, actor).put("src", sourceName);
                            } catch (IllegalArgumentException ignored){
                                // java.lang.IllegalArgumentException: loops not allowed
                            }
                        }
                    }


                    /*for (Range r : ranges) {
                        String type = r.getProps().get("type");
                        if("verb".equals(type)){
                            PropVertex verb = new PropVertex(normalizer.normalize(r.getValue()));
                            verb.getProps().put("type", "verb");
                            verb.getProps().put("src", sourceName);


                            if(!g.containsVertex(verb)) {
                                g.addVertex(verb);
                            }
                            for (PropVertex actor : actors) {
                                PropWeightedEdge e = g.addEdge(verb, actor);
                                g.getEdge(verb, actor).put("snippet", highlight);
                                g.getEdge(verb, actor).put("src", sourceName);

                            }
                            for (PropVertex loc : locs) {
                                PropWeightedEdge e = g.addEdge(verb, loc);
                                g.getEdge(verb, loc).put("snippet", highlight);
                                g.getEdge(verb, loc).put("src", sourceName);

                            }
                        }
                    }*/
                }
            });
        }
        histograms.values().forEach(h -> {h.retainTopEntries(250); h.removeLowOccurences(3);} );
        /*Map<String, Map<String, Double>> freqMaps = new HashMap<>();
        for (Map.Entry<String, Histogram<String>> entry : histograms.entrySet()) {
            freqMaps.put(entry.getKey(), entry.getValue().removeLowOccurences(2).asSortedMap(100));
        }*/

        return tuple(result.setProfile(profile).setWordFrequencies(histograms), g);
    }


}
