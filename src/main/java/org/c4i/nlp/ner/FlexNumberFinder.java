package org.c4i.nlp.ner;

import org.c4i.nlp.StopWords;
import org.c4i.nlp.match.EntityPlugin;
import org.c4i.nlp.match.Literal;
import org.c4i.nlp.match.Range;
import org.c4i.nlp.normalize.StringNormalizer;
import org.c4i.nlp.tokenize.RegexUtil;
import org.c4i.nlp.tokenize.Token;
import org.c4i.nlp.tokenize.Tokenizer;
import com.google.common.collect.ImmutableList;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Find number values
 * @author Arvid Halma
 * @version 17-03-2019
 */
public class FlexNumberFinder implements EntityPlugin {
    DataSheet cardinalNumbers;
    RegexFinder numberFinder;
    Pattern digitStart = Pattern.compile("^(\\d+)\\D+");

    public FlexNumberFinder(File file, StringNormalizer normalizer, Tokenizer tokenizer) throws IOException {
        this.cardinalNumbers = new DataSheet("CARDINALNUMBER", file, normalizer, tokenizer, StopWords.EMPTY_STOPWORDS);
        this.numberFinder = new RegexFinder(RegexUtil.NUMBER, "number");
    }

    @Override
    public boolean accept(Literal lit) {
        return lit.equals("FLEXNUMBER");
    }

    @Override
    public List<Range> find(Token[] tokens, Literal lit, String label, int location, Collection<Range> context) {
        Token t = tokens[location];
        Matcher digitStartMatcher = digitStart.matcher(t);
        if(digitStartMatcher.find()){
            // e.g. 12-year old
            Range range = new Range(label,
                    location, location+1,
                    t.getCharStart(), t.getCharEnd());
            range.props.put("type", "flexnumber");
            range.props.put("number", digitStartMatcher.group(1));
            return ImmutableList.of(range);
        }

        List<Range> ranges = new ArrayList<>(0);
        boolean findMore = true;
        while (findMore) {
            if(location >= tokens.length)
                break;

            int startLoc = location;
            List<Range> cardinals = cardinalNumbers.find(tokens, lit, label, location, context);
            if(!cardinals.isEmpty()){
                // cardinal numbers found
                ranges.addAll(cardinals);
                location = cardinals.get(cardinals.size()-1).tokenEnd;
            }

            if(location >= tokens.length)
                break;

            List<Range> numbers = numberFinder.find(tokens, lit, label, location, context);
            if(!numbers.isEmpty()){
                // cardinal numbers found
                ranges.addAll(numbers);
                location = numbers.get(numbers.size()-1).tokenEnd;
            }

            findMore = startLoc != location;
        }

        if(!ranges.isEmpty()) {
            try {
                Range firstRange = ranges.get(0);
                Range lastRange = ranges.get(ranges.size() - 1);

                double x = Double.NaN;
                double lastPart = Double.NaN;
                for (int i = 0; i < ranges.size(); i++) {
                    Range range = ranges.get(i);
                    double part = Double.parseDouble(range.props.get("number"));
                    if (i == 0) {
                        x = part;
                        lastPart = x;
                    } else {
                        if(part < lastPart) {
                            // e.g. hundred five
                            x += part;
                        } else {
                            // e.g. five hundred
                            x *= part;
                        }
                        lastPart = part;
                    }
                }
                Range range = new Range(label,
                        firstRange.tokenStart, lastRange.tokenEnd,
                        firstRange.charStart, lastRange.charEnd);
                range.props.put("type", "flexnumber");
                range.props.put("number", Double.toString(x));

                return ImmutableList.of(range);
            } catch (Exception e){
                return ranges;
            }
        } else {
            return ImmutableList.of();
        }

    }

    @Override
    public String description() {
        return "FLEXNUMBER";
    }

    @Override
    public String toString() {
        return "FlexNumberFinder{}";
    }
}
