package org.c4i.nlp;

import org.c4i.nlp.normalize.StringNormalizer;
import org.c4i.nlp.normalize.StringNormalizers;
import org.c4i.nlp.tokenize.MatchingWordTokenizer;
import org.c4i.nlp.tokenize.Token;
import org.c4i.util.LineParser;
import com.google.common.collect.ImmutableList;

import java.io.File;
import java.io.IOException;
import java.text.Normalizer;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Stop word removal.
 * @author Arvid
 * @version 6-4-2015 - 11:58
 */
public class StopWords implements StringNormalizer, WordTest{
    private Set<String> stopWords;
    private List<StringNormalizer> normalizers;
    public final static StopWords EMPTY_STOPWORDS = new StopWords();

    private StopWords(){
        this.stopWords = Collections.emptySet();
        this.normalizers = ImmutableList.of(StringNormalizer.IDENTITY);
    }

    /**
     * Creates a list of stopwords from the entries in a file. Adds all normalized versions of a word.
     * @param stopWordFile the file containing stopwords, 1 per line
     * @param normalizers a list of <b>more conservative</b> (e.g. {@link StringNormalizers#DEFAULT} to <b>more aggressive</b> normalizers (e.g. {@link org.c4i.nlp.normalize.phonetic.Metaphone3}.
     * @throws IOException
     */
    public StopWords(File stopWordFile,  List<StringNormalizer> normalizers) throws IOException {
        this.normalizers = normalizers.isEmpty() ? this.normalizers = ImmutableList.of(StringNormalizer.IDENTITY) : normalizers;
        this.stopWords = new HashSet<>();
        LineParser.lines(line -> {
            for (StringNormalizer normalizer : normalizers) {
                String nword = normalizer.normalize(line);
                if (!nword.isEmpty())
                    stopWords.add(nword);
            }
        }, stopWordFile);
    }

    public void addAll(List<String> words){
        for (String word : words) {
            for (StringNormalizer normalizer : normalizers) {
                String nword = normalizer.normalize(word);
                if (!nword.isEmpty())
                    stopWords.add(nword);
            }
        }
    }

    public Set<String> getStopWords() {
        return stopWords;
    }

    @Override
    public boolean test(String word) {
        return !isStopWord(word);
    }

    public boolean isStopWord(String word){
        String nword = normalizers.get(0).normalize(word);
        return stopWords.contains(nword);
    }

    public boolean isStopWord(Token token){
        String nword = token.isNormalized() ? token.getNormalizedWord() : normalizers.get(0).normalize(token.getWord());
        return stopWords.contains(nword);
    }

    public List<Boolean> isStopWord(Collection<String> words){
        return words.stream().map(this::isStopWord).collect(Collectors.toList());
    }

    public List<Boolean> isStopWordTokens(Collection<Token> tokens){
        return tokens.stream().map(this::isStopWord).collect(Collectors.toList());
    }

    public Predicate<String> stopWordPredicate(){
        return this::isStopWord;
    }

    public Collection<String> filter(Collection<String> words){
        words.removeIf(this::isStopWord);
        return words;
    }

    public List<String> filter(List<String> words){
        words.removeIf(this::isStopWord);
        return words;
    }

    public Collection<Token> filterTokens(Collection<Token> words){
        words.removeIf(this::isStopWord);
        return words;
    }

    public List<Token> filterTokens(List<Token> words){
        words.removeIf(this::isStopWord);
        return words;
    }

    @Override
    public String normalize(String string) {
        return new MatchingWordTokenizer().tokenize(string).stream()
                .filter(not(this::isStopWord)).map(Token::getWord).collect(Collectors.joining(" "));
    }

    public static <T> Predicate<T> not(Predicate<T> t) {
        return t.negate();
    }

    public String description(){
        return stopWords.size() + " stop words";
    }

    @Override
    public String toString() {
        return "StopWords" + stopWords;
    }
}
