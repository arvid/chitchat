<#import "utils.ftl" as u>

<#macro pageTitle>
ChitChat Speech Demo
</#macro>

<#macro pageContent>

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">
                Speech Demo
            </h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="/api/v1/ui/dashboard" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">System</span>
                    </a>
                </li>
                <li class="m-nav__separator">
                    -
                </li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Speech Demo</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="m-content">
    <div class="row">
        <div class="col-lg-12" >
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="la la-volume-up"></i>
												</span>
                            <h3 class="m-portlet__head-text">
                                Speech synthesis demo
                            </h3>
                        </div>
                    </div>

                </div>
                <div class="m-portlet__body">
                    <p class="alert alert-dismissible fade show" id="msg"></p>


                    <div class="form-group m-form__group">
                        <label>
                            Text
                        </label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="speech-msg" id="speech-msg" placeholder="Say this...">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button" id="speak">
                                    Speak
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="voice">Voice</label>
                        <select class="form-control m-input" name="voice" id="voice"></select>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="lang">Language</label>
                        <select class="form-control m-input" name="lang" id="lang"></select>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="volume" style="width: 100px;">Volume</label>
                        <input type="range" min="0" max="1" step="0.1" name="volume" id="volume" value="1">
                    </div>
                    <div class="form-group m-form__group">
                        <label for="rate" style="width: 100px;">Rate</label>
                        <input type="range" min="0.1" max="10" step="0.1" name="rate" id="rate" value="1">
                    </div>
                    <div class="form-group m-form__group">
                        <label for="pitch" style="width: 100px;">Pitch</label>
                        <input type="range" min="0" max="2" step="0.1" name="pitch" id="pitch" value="1">
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

</#macro>

<#macro pageScript>

<script>
    $.hx.setCurrentPage('#menu-item-system-speech')

    /*
     * Check for browser support
     */
    const supportMsg = document.getElementById('msg');

    if ('speechSynthesis' in window && speechSynthesis.getVoices().length) {
        $(supportMsg).addClass('alert-success')
        supportMsg.innerHTML = 'Your browser <strong>supports</strong> speech synthesis.';
    } else {
        $(supportMsg).addClass('alert-danger')
        supportMsg.innerHTML = 'Sorry your browser <strong>does not support</strong> speech synthesis.';
        supportMsg.classList.add('not-supported');
    }


    // Get the 'speak' button
    const button = document.getElementById('speak');

    // Get the text input element.
    const speechMsgInput = document.getElementById('speech-msg');

    // Get the voice select element.
    const voiceSelect = document.getElementById('voice');
    const langSelect = document.getElementById('lang');

    // Get the attribute controls.
    const volumeInput = document.getElementById('volume');
    const rateInput = document.getElementById('rate');
    const pitchInput = document.getElementById('pitch');


    // Fetch the list of voices and populate the voice options.
    function loadVoices() {
        // Fetch the available voices.
        const voices = speechSynthesis.getVoices();
        const langs = new Set();

        const option = document.createElement('option');
        // option.value = "";
        option.innerHTML = "-";
        voiceSelect.appendChild(option);

        // Loop through each of the voices.
        voices.forEach(function(voice, i) {
            // Create a new option element.
            const option = document.createElement('option');

            // Set the options value and text.
            option.value = voice.name;
            option.innerHTML = voice.name;
            langs.add(voice.lang);

            // Add the option to the voice selector.
            voiceSelect.appendChild(option);
        });

        langs.forEach(l => {
            const option = document.createElement('option');

            // Set the options value and text.
            option.value = l;
            option.innerHTML = l;

            // Add the option to the voice selector.
            langSelect.appendChild(option);
        })
    }

    // Execute loadVoices.
    loadVoices();

    // Chrome loads voices asynchronously.
    window.speechSynthesis.onvoiceschanged = function(e) {
        loadVoices();
    };


    // Create a new utterance for the specified text and add it to
    // the queue.
    function speak(text) {
        // Create a new instance of SpeechSynthesisUtterance.
        const msg = new SpeechSynthesisUtterance();

        // Set the text.
        msg.text = text;

        // Set the attributes.
        msg.volume = parseFloat(volumeInput.value);
        msg.rate = parseFloat(rateInput.value);
        msg.pitch = parseFloat(pitchInput.value);

        // If a voice has been selected, find the voice and set the
        // utterance instance's voice attribute.
        if (voiceSelect.value) {
            msg.voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name === voiceSelect.value; })[0];
        }

        // Set language
        if (langSelect.value) {
            msg.lang = langSelect.value;
        }

        // Queue this utterance.
        window.speechSynthesis.speak(msg);
    }


    // Set up an event listener for when the 'speak' button is clicked.
    button.addEventListener('click', function(e) {
        if (speechMsgInput.value.length > 0) {
            speak(speechMsgInput.value);
        }
    });


</script>

</#macro>