<#macro pageContent>
<!DOCTYPE html>
<!--
   ________    _ __  ________          __
  / ____/ /_  (_) /_/ ____/ /_  ____ _/ /_
 / /   / __ \/ / __/ /   / __ \/ __ `/ __/
/ /___/ / / / / /_/ /___/ / / / /_/ / /_
\____/_/ /_/_/\__/\____/_/ /_/\__,_/\__/

Leiden university - Centre for Innovation - HumanityX

-->
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        ChitChat Client
    </title>
    <meta name="description" content="Chat with us!">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="/assets/app/js/chitchatclient.core.css">
    <link rel="stylesheet" href="/assets/app/js/chitchatclient.components.css">

    <style>
        html, body {
            box-sizing: border-box;
            height: 100%;
            width: 100%;
            padding: 0;
            margin: 0;
        }

        body {
            background-color: #ebedf4;
            font-family: "Open Sans", sans-serif;
            display: flex;
            flex-direction: column;
        }

        #welcome {
            min-width: 400px;
            max-width: 800px;
            width: 50vw;
            margin: 5px auto 0 auto;
            display: flex;
            align-items: center;
            justify-items: left;
        }

        #chitchatclient-title {
            color: var(--chat-right-bg-color);
            font-size: 40px;
            font-weight: 200;
        }

        #chitchatclient-logo {
            max-width: 60px;
            max-height: 60px;
            min-height: 60px;
            margin-right: 20px;
        }

        #chitchatclient-description {
            color: var(--chat-right-bg-color);
            margin-left: auto;
        }

        #chitchatclient-tools {
            margin-left: auto;
        }

        #chitchatclient {
            flex: 1;
            max-width: 800px;
            min-width: 400px;
            width: 50vw;
            min-height: 400px;
            margin: 20px auto 50px auto;

            background-color: white;
            border-radius: 20px;
            box-shadow: 0 5px 15px 1px rgba(69,65,78,.15)
        }

    </style>

    <link rel="shortcut icon" href="/assets/app/media/img/favicon.ico"/>
</head>

<body>
<div id="welcome">
    <img id="chitchatclient-logo" alt="" src="/assets/app/media/img/chitchat-logo2.svg"/>
    <div id="chitchatclient-title">ChitChat</div>
    <div id="chitchatclient-description" style="display: none"></div>
    <div id="chitchatclient-tools">
        <a href="#" class="btn-round chitchat-reset" style="margin-left: 10px; width: 20px; padding-top: 3px;">&#8635;</a>
    </div>
</div>

<div id="chitchatclient"></div>
<script src="/assets/app/js/chitchatclient.polyfill.js" type="application/javascript"></script>
<script src="/assets/app/js/chitchatclient.util.js" type="application/javascript"></script>
<script src="/assets/app/js/chitchatclient.core.js" type="application/javascript"></script>
<script src="/assets/app/js/chitchatclient.rich.js" type="application/javascript"></script>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var parent = document.getElementById('chitchatclient');

        var userId;
        var rememberMe = true;

        if(rememberMe){
            userId = localStorage.getItem("chitchatUserId")
            if(!userId){
                userId = 'user-' + randomString(8)
            }
            localStorage.setItem("chitchatUserId", userId)
        } else {
            userId = 'user-' + randomString(8)
        }

        var chitChatClient = new RichChitChatClient(parent, {
            title: 'ChitChat',
            chitChatHost: '',
            stylePath: '/assets/app/js/',
            userId: userId,
        });

    });
</script>
</body>
</html>
</#macro>