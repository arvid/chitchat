<#import "utils.ftl" as u>

<#macro pageTitle>
    ChitChat - Privacy
</#macro>

<#macro pageContent>

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Privacy Policy
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="/api/v1/ui/dashboard" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Privacy
											</span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">

                <p>Last Updated: 25 May 2018</p>
                <p>Your privacy and the privacy of the data subjects of any data we handle is important to the Centre for Innovation, Leiden University. The aim of this privacy policy is to explain what data we collect through our interactions with you, for what purposes, and the steps we take to protect it.</p>
                <p><strong>Who is the Centre for Innovation?</strong></p>
                <p>The Centre for Innovation (“Centre”; “we”; “our”) is a part of Leiden University (“University”) in the Netherlands. The Centre aims to prepare the university for the digital future by exploring the juncture of education, technology and society.</p>
                <p>Within the Centre there are three labs, namely, the Digital Learning Lab, HumanityX&nbsp; and the Future Work Lab. These labs conduct projects within the university, with outside partners, and host or organise events in collaboration with partners and on our own. All of these labs is covered by this privacy policy.</p>
                <p><strong>Core beliefs regarding user privacy and data protection</strong></p>
                <p>At the Centre we strive to put people first in a digital age. Therefore, we aim to ensure that our data policy ensures the highest level of respect for data subjects and in turn their data. We acknowledge that personal data is an integral part of a person’s identity and therefore, treat all such data with highest level of integrity and confidentiality.</p>
                <p><strong>Relevant legislation and documents that have been taken into account</strong></p>
                <p>The Centre legally complies with the General Data Protection Regulation (GDPR) of the European Union (EU), but also aspires to meet the standard that have been outlined in the work of our various partners (such as the Signal Code written by the Signal Program at Harvard Humanitarian Initiative and the International Committee of the Red Cross’ Handbook on Data Protection in Humanitarian Action).</p>
                <p><strong>Data collection and processing</strong></p>
                <p>In keeping with the spirit of data minimisation, the Centre aims to collect as little personal data as possible and only that data which is necessary for us to carry out our work.We collect information that will be outlined in the policy below. Personal data is only collected if informed consent is given by the data subject.</p>
                <p>The Centre applies the principles of transparency, purpose limitation and data minimisation when processing data we have collected or data that has been shared with us. Therefore, all data is processed lawfully, fairly and in a transparent manner. We aim to ensure that data subjects are aware of when and why data is collected about them as well as the manner in which it will be processed. This information will accessible to data subjects, either online through our Privacy Policy or through direct request from the Data Protection Officer of the Centre.</p>
                <p>The Centre only collects the data which it finds absolutely necessary to complete the specified task. The tasks for which data is collected can be found in this document or in the project documents. Should the Centre wish to use personal data for a task other than the one that was specified when consent was given, renewed consent from the data subject will be sought for the new task. Only the data for which the Centre has obtained provable consent will then be stored and processed.</p>
                <p>Projects - Data may be collected on a project basis. Data collected for a specific project will be collected, processed and stored in compliance with the GDPR, this Privacy Policy, and any other specifications set out in the project documents.</p>
                <p>Sharing - Any personal data held by the Centre will only be used for the purposes for which it was obtain and will not be shared or sold to any third parties. Furthermore, this data will not undergo further processing, unless consent has been obtained.</p>
                <p><strong>Data Storage</strong></p>
                <p>When storing data that the Centre has collected or received, we aim to ensure that the appropriate technical and organisational measures are taken to guarantee the appropriate level of protection is maintained throughout the retention period and in the process of deletion. It must be noted that the Centre aims to only store personal data that it deems absolutely necessary to carry out its work, all other data is stored anonymously.</p>
                <p>Anonymisation -&nbsp; The Centre does not store personal data unless necessary to complete a task. All data is received already anonymised, or anonymised by the Centre if the personal aspects are not necessary.</p>
                <p>Sharing - Any personal data held by the Centre will only be used for the purposes for which it was obtain and will not be shared or sold to any third parties. Furthermore, this data will not undergo further processing, unless consent has been obtained.</p>
                <p>Retention - The standard retention period for any data stored by the Centre is 3 months after the end of the purpose for which the data was collected is completed or has ended. This period may differ according to the specifications in project documents.</p>
                <p>Requesting your data - To request a copy of your data please send an email to the Data Protection Officer of the Centre. He/she will then provide you with a copy of your data that is controlled by the Centre, but will not be able to provide the data controlled by Leiden University. You may also request to have your personal data updated. If you would like to request or update your data stored by the University, please contact Leiden University’s Data Protection Officer.</p>
                <p>Deletion - You may request your data be deleted at any point by contacting the Data Protection Officer of the Centre. Your data will then be deleted within a reasonable timeframe.</p>
                <p><strong>Changes to Privacy Policy</strong></p>
                <p>The Centre may update this Privacy Policy from time to time. The date of the most recent revision will appear at the top of the policy. If you do not agree with any changes that have been made, please do not submit personal data to the Centre and send a request for all your personal data to be deleted. Should any material changes take place regarding personal data, notification will be sent out via email as well as appear in the change log.</p>

                <p><br>
                    <strong>Data Protection Officer<br>
                        Centre for Innovation</strong></p>
                <p>Joanna van der Merwe<br>
                    Email: j.s.van.der.merwe@fgga.leidenuniv.nl<br>
                    Phone: +31 (0) 70 800 9341</p>
                <p><strong>Leiden University</strong></p>

            </div>
        </div>
    </div>
</#macro>

<#macro pageScript>
    <script>

    </script>
</#macro>