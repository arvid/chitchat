// Channel select

function createChannelSelect(element = '#channelSelect', hxVar = 'channel', onChange = ()=>{}) {

    const channelSelect = $(element);
    channelSelect.html(''); // clear
    $.when($.get('/api/v1/db/channel'), $.get('/api/v1/db/textdoc/type/doc')).done(function (channels, doctypes) {
        let optgroup = $('<optgroup label="Chats"/>')
        channels[0].forEach((ch, ix) => {
            optgroup.append($("<option />").val(ch).text(ch));
        });
        channelSelect.append(optgroup)

        optgroup = $('<optgroup label="Documents"/>')
        doctypes[0].forEach((dt) => {
            optgroup.append($("<option />").val(dt).text(dt));
        });
        channelSelect.append(optgroup)

        channelSelect
            .val($.hx.get(hxVar, ''))
            .change(function () {
                let selectedVal = $(this).find(":selected").val();
                $.hx.set(hxVar, selectedVal)
                onChange()
            })
    })
}
