package org.c4i.util;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * @author Arvid Halma
 * @version 19-8-19
 */
public class CombinatoricsDemo {
    public static void main(String[] args) {
        List<Integer> as = ImmutableList.of(1, 2, 3);
        System.out.println("as = " + as);
        System.out.println("Combinatorics.subLists(as) = " + Combinatorics.subLists(as));
        as = ImmutableList.of();
        System.out.println("as = " + as);
        System.out.println("Combinatorics.subLists(as) = " + Combinatorics.subLists(as));

    }
}
