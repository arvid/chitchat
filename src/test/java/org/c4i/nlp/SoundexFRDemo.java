package org.c4i.nlp;

import org.c4i.nlp.normalize.phonetic.Metaphone3;
import org.c4i.nlp.normalize.phonetic.Sonnex;
import org.c4i.nlp.normalize.phonetic.Soundex2FR;
import org.c4i.nlp.normalize.phonetic.SoundexFR;

/**
 * @author Arvid Halma
 * @version 01-11-19
 */
public class SoundexFRDemo {

    public static void main(String[] args) {

        String[] words = {
            "balade",  "ballade",
            "basilic",  "basilique",
            "boulot",  "bouleau",
            "cane",  "canne",
            "censé",  "sensé",
            "compte",  "comte",  "conte",
            "cygne",  "signe",
            "date",  "datte",
            "dessin",  "dessein",
            "différend",  "différent",
            "cric",  "crique",
            "champ",  "chant"
        };

        SoundexFR soundexFR = new SoundexFR();
        Soundex2FR soundex2FR = new Soundex2FR();
        Sonnex sonnex = new Sonnex();
        Metaphone3 meta3 = new Metaphone3();
        System.out.println("Word:       sonnex:     soundex:    soundex2:   metaphone3:  ");
        for (String word : words) {
//            System.out.printf("%12s%12s%12s%12s%12s\n", word,
            System.out.printf("%s\t%s\t%s\t%s\t%s\t\n", word,
                    sonnex.normalize(word), soundexFR.normalize(word), soundex2FR.normalize(word), meta3.normalize(word));
        }

    }
}
