package org.c4i.nlp;

import org.c4i.nlp.normalize.SnowballStemmer;
import org.c4i.nlp.normalize.StringNormalizer;
import org.c4i.nlp.normalize.StringNormalizers;
import org.c4i.nlp.normalize.phonetic.MetaphoneES;
import org.c4i.util.LineParser;

import java.io.File;
import java.io.IOException;

public class StopwordClash {
    public static void main(String[] args) throws IOException {
        StringNormalizer normalizer = StringNormalizers.DEFAULT
                .andThen(new SnowballStemmer("es"))
                .andThen(new MetaphoneES());
        LineParser.lines(line -> {
                String nword = normalizer.normalize(line);
                if("OYK".equals(nword)){
                    System.out.println("word  = " + line);
                    System.out.println("nword = " + nword);
                }
        }, new File("data/nlp/es/stopwords_es.txt"));
    }
}
